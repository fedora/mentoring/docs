= Ideas: Google Summer of Code 2012
Find an idea you like? Want to propose your own? See the Getting Started
Guide with GSoC:

link:index.html[Information]
////
You may be interested in ideas from
link:Summer_coding_ideas_for_2011[2011],
link:Summer_coding_ideas_for_2010[2010],
link:Summer_coding_ideas_for_2009[2009] and
link:Summer_coding_ideas_for_2008[2008].
////
Further, last year accepted ideas from the Fedora Project can be found
at http://www.google-melange.com/gsoc/org/google/gsoc2011/fedora[GSoC
2011 web site]

[[students-welcome]]
== Students Welcome

If you are a student looking forward to participate the GSoC 2012 with
Fedora, please feel free to browse the idea list which is still growing.
Do not hesitate to contact the mentors/ contributors as indicated in
this page for any related clarification. If you are new to The Fedora
project, following material would help you to get started. Further
please sign-up with the https://fedoraproject.org/wiki/FAS[Fedora
Account System(FAS)] if you are willing to continue with the Fedora
project. `#fedora-devel`, IRC channel can be used to get instant
support.
1.  link:https://fedoraproject.org/wiki/Foundation[The Foundation]
2.  http://docs.fedoraproject.org/en-US/index.html[Fedora Documentation]
3.  link:https://fedoraproject.org/wiki/Communicate/IRCHowTo[IRC]
4.  https://fedoraproject.org/wiki/FAS[Fedora Account System(FAS)]
5.  link:https://fedoraproject.org/wiki/Development[Development]

[[supporting-mentors]]
== Supporting Mentors

Following contributors are also willing to support the GSoC 2012
program. (please feel free to add your self, attach the user page).
Sometimes there should be some backing up mentors to mentor if the
original mentor get busy with something for a short time period. In such
case we need help.

1.  link:https://fedoraproject.org/wiki/User:Bckurera[Buddhike Kurera(Bckurera)]
2.  link:https://fedoraproject.org/wiki/User:Quaid[Karsten Wade(Quaid)]
3.  link:https://fedoraproject.org/wiki/User:Susmit[Susmit Shannigrahi(Susmit)]
4.  link:https://fedoraproject.org/wiki/User:Duffy[ Mo Duffy (Duffy)]
5.  link:https://fedoraproject.org/wiki/User:Mmorsi[ Mo Morsi]

[[draft-of-an-idea]]
== Draft of an idea

Please add your idea as follows.

[[project-name]]
=== Project name

_Status:_

_Summary of idea:_

_Knowledge prerequisite:_

_Skill level:_

_Contacts:_

_Mentor(s):_

_Notes:_

*!!!The draft was changed slightly, please add required field as
required!!!*

[[idea-list-for-gsoc-2012]]
== Idea list for GSoC 2012

[[applications-for-desktop-end-users]]
=== Applications for desktop end users

These are coding projects that benefit end users of the Linux desktop.

[[integrate-proxy-settings-and-network-connectionslocations]]
==== Integrate Proxy Settings and Network Connections(Locations)

_Status:_ Proposed

_Summary of idea:_ The system should use an appropriate networking
profile (e.g. Proxy settings) for each network connection.

Gnome 2 had a concept of network locations in its Network Proxy
configuration window. However, user should selected the appropriate
location whenever he moves between networks. This idea is about
providing an integration between NetworkManager and Desktop environments
so that a user can create network profiles for each network
location(connection) providing appropriate settings like proxy settings
which is the main proposed setting here. NetworkManager can have a
"Network Location" concept: for wireless networks, usually the name of
the network (ESSID) is usually enough. For wired connections, DHCP
servers can and usually do provide network's domain name, which can be
used as the name of the location. It is nice if a user can associate
each network location with a network settings profile which will be used
whenever the user is connected to that network automatically. So, when
you connect to a network, a corresponding network settings profile is
activated automatically.

_Knowledge prerequisite:_

_Skill level:_

_Contacts:_ link:https://fedoraproject.org/wiki/User:Hedayat[Hedayat Vatankhah]

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:danw[Dan Winship]

_Notes:_ There is an entry with some description in
http://cgit.freedesktop.org/NetworkManager/NetworkManager/tree/TODO[NetworkManager
TODO] which should be considered for the implementation

[[bringing-the-cloud-to-the-fedora-desktop]]
==== Bringing the Cloud to the Fedora Desktop

_Status:_ Proposed

_Summary of idea:_ http://www.aeolusproject.org[Aeolus] is an umbrella
project that provides an open source API which to control any number of
backend proprietary cloud providers. It allows us to write tooling that
is able to deploy, monitor, and manager instances to any cloud provider
such as EC2 or OpenStack in a Free and Open Manner. We need more tooling
to interface with the various Aeolus components from the Fedora desktop
(or any other) in novel ways such as command-line and gtk-based
applications to deploy instances to the cheapest cloud provider or the
one with the fastest response time, to monitor running instances using
various visualizations, and more closely integrate local data and code
w/ that on the cloud.

_Knowledge prerequisite:_

_Skill level:_

_Contacts:_ link:https://fedoraproject.org/wiki/User:_Mmorsi[Mo Morsi]

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:_Mmorsi[Mo Morsi], link:https://fedoraproject.org/wiki/User:_Matthewwagner[ Matt Wagner]
as co-mentor

_Notes:_ Ping me (mmorsi on freenode) for more info about the Aeolus
project. All the components which to build images for the cloud and
control instances is in place and ready to go, just drop various bits
(they are interoperable and interchangable) into an environment to be
able to interact with the cloud, avoiding proprietary interfaces and
vendor lockin.

[[web-hosting-control-panel]]
==== Web hosting control panel

_Status:_ Proposed - Draft

_Summary of idea:_ develop a free alternative of cpanel / plesk control
panels, 100% compatible with fedora, and redhat enterprise Linux.
written in python.

the control panel will be able to create domains and automatically setup
apache, postfix, dovecot, mysql, postgresql bind etc...

_Knowledge prerequisite:_ apache, postfix, dovecot, mysql, postgresql,
proftpd, bind

_Skill level:_ Medium

_Contacts:_ itamarjp [AT] fedoraproject [DOT] org, kaustubh [DOT]
karkare [AT] gmail [DOT] com

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:Ausil[ Dennis Gilmore]

_Notes:_ link:https://fedoraproject.org/wiki/User:Itamarjp/gsoc-2012[webhosting control panel draft]

[[assemble-a-toolchain-for-recording-screencasts-easily]]
==== Assemble a toolchain for recording screencasts easily

_Status:_ Proposed - Draft - Need a mentor

_Summary of idea:_ Creating screencasts on Fedora and post-processing
them for upload should be easy and fool proof.

The link:https://fedoraproject.org/wiki/Videos[Fedora Videos] project has been launched to bring
together a collection of screencasts that help people learn how to use
Fedora. There's only one major hurdle, though. Capturing really good
screencasts on Fedora, post-processing them to include an intro and
outro and other effects, and finally exporting them to an open format is
challenging. There are lots of different tools, but each one only gets
you part of the way there and you have to be the glue to make it all
work. And then there are the bugs.

The goal of this project is to assemble a toolchain (a recommended
collection of software available in the Fedora repositories) and write a
comprehensive tutorial for how to create an professional-looking
screencast. That may included chasing down & fixing bugs in the existing
tools, discovering new tools and getting them packaged and learning
about techniques in the environment around the computer, such as how to
select & setup a microphone or how to draft a good speaking script. One
possible task in the project is to add sound recording support to the
Gnome 3 desktop recorder. Currently, the desktop recorder only grabs the
video of the screen.

The student will get support and advice from the Fedora Videos team
since they are trying to learn how to create these screencasts.

_Knowledge prerequisite:_ gstreamer, video and sound editing, blender

_Skill level:_ Medium

_Contacts:_ link:https://fedoraproject.org/wiki/Videos[Fedora Videos Team]

_Mentor(s):_ -

_Notes:_

[[improve-fedoras-tablet-user-experience]]
==== Improve Fedora's Tablet User Experience

_Status:_ Proposed - Draft

_Summary of idea:_ Improving Fedora's Tablet User Experience.

The window managers/desktop environments officially supported by Fedora
do not match the expectations of people with low power touch devices.

Fedora features new technology to reduce system load and - requirements,
the user interface has not seen too much love.

Currently available interfaces for users are:

* Too Ressource Consuming
* Too Overloaded or
* Too Feature Poor
* Not Customizable With Reasonable Effort

The E17 Desktop Shell, developed as part of the
http://www.enlightenment.org[Enlightenment Project], is a perfect match.
It provides a rich interface, whilst consuming very little ressources.
The Enlightenment Foundation Libraries, on which E17 is built upon, were
chosen as Foundation Block of Tizen, the MeeGo successor backed by
Samsung and Intel. If things work out, E17 will be the first "real"
Desktop Shell for Wayland.

Though it already provides
http://dev.enlightenment.fr/~captainigloo/2011/08/25/elfe-and-the-composited-windows-list/[special
behaviour] for touchscreen devices through certain modules, there's
still room for improvement.

The overall goal of this project would be to improve the user experience
of people who want to use Fedora on low power devices such as tablets.

The student will get support and advice from the Enlightenment Project
developers.

_Knowledge prerequisite:_ C, X11, Wayland, Enlightenment Foundation
Libraries

_Skill level:_ Medium to High

_Contacts:_ Michael Bouchaud (yoz [at] efl [dot] so), link:https://fedoraproject.org/wiki/User:leif[ Leif
Middelschulte] (leif [dot] middelschulte [at] student [dot] kit [dot]
edu)

_Mentor(s):_ Michael Bouchaud (yoz [at] efl [dot] so)

_Notes:_ If this idea gets realized, a proper tablet spin can be
created, that honors the spirit of Fedora.

[[applications-for-programmers]]
=== Applications for programmers

[[implement-a-binding-translator-for-glusterfs]]
==== Implement a binding translator for GlusterFS

_Status:_ Proposed - Draft

_Summary of idea:_ In transporting data across networks, programmers
need GlusterFS language bindings to be able to create apps in their
language of choice. A language binding translator would greatly increase
the number of programmers who would be able to more easily extend
GlusterFS. It's also a fun project for someone wanting to get some
experience with multi-threaded architecture and design in the context of
distributed systems and data.

_Knowledge prerequisite:_ C, Python, embedded Python

_Skill level:_ Medium to High

_Contacts:_ johnmark [AT]redhat [DOT] com , abperiasamy [AT] gmail [DOT]
com

_Mentor(s):_ Anand Avati - Mentor, AB Periasamy - Co-mentor,
link:https://fedoraproject.org/wiki/User:johnmark[John Mark Walker] - Co-mentor

_Notes:_ -

[[implement-a-cassandranosql-connector-or-translator-for-glusterfs]]
==== Implement a Cassandra/NoSQL Connector or Translator for GlusterFS

_Status:_ Proposed - Draft

_Summary of idea:_ In general, there is a need for the ability to store
and access NoSQL data on general purpose distributed filesystems.
Currently, there are a host of single-purpose methods for storing and
retrieving NoSQL data, which are difficult to access from legacy
applications. Creating a NoSQL translator for GlusterFS would help
mitigate this problem and give developers more options for storing and
accessing "big data" in a way that is accessible via a variety of
standard toolkits and protocols.

_Knowledge prerequisite:_ C, GlusterFS internals

_Skill level:_ High

_Contacts:_ johnmark [AT]redhat [DOT] com , abperiasamy [AT] gmail [DOT]
com

_Mentor(s):_ Anand Avati - Mentor, AB Periasamy - Co-mentor,
link:https://fedoraproject.org/wiki/User:johnmark[John Mark Walker] - Co-mentor

_Notes:_ -

[[linux-kernel-project]]
=== Linux kernel project

[[implement-nfsfscache-writeback-cache]]
==== Implement nfs/fscache writeback cache

_Status:_ Proposed

_Summary of idea:_ Currently, the flashcache/bcache only works for local
filesystems. For network filesystems, nfs only supports write through
cache based on fscache/cachefiles. With wider adoption of SSD on nfs
client side, it would be great for nfs to support writeback cache to
speed up write intensive clients. This project would implement a
writeback mechanism for nfs, which need to make necessary changes to
kernel nfs, fscache and cachefiles modules, and take care of nfs
close-to-open semantics.

_Knowledge prerequisite:_ C, Linux kernel, nfs

_Skill level:_ High

_Contacts:_ bergwolf [AT] gmail [DOT] com

_Mentor(s):_ Peng Tao - Mentor

[[infrastructure-for-fedora-contributors-and-users]]
=== Infrastructure for Fedora contributors and users

[[implement-a-survey-infrastructure-for-the-fedora-project]]
==== Implement a survey infrastructure for the Fedora Project

_Status:_ Proposed - Draft

_Summary of idea:_ Surveys are important to increase the quality of a
service, and it is true for the Fedora project as well.(discussions
http://meetbot.fedoraproject.org/fedora-meeting/2011-12-24/apac.2011-12-24-04.00.log.html#l-310[1])
In this project it is supposed to implement a survey infrastructure
facility so that the contributors can use for various activities as per
the need.

The concern is to develop a simple web base survey system OR implement a
existing survey project and customized as required, so that contributors
can easily create surveys and dig for results as well. Linking with the
[FAS|FAS] is required. Further it should be compatible with anonymous
and open ID so that if the creator set for those authentication the
survey should allow those authentications. Statistics and other required
reports can be obtained. So that it is easy to analyse.

_Knowledge prerequisite:_ PHP, Python, Ruby are preferred, MySQL
(database handling), experience with Lime Survey would be an advantage

_Skill level:_ Medium

_Contacts:_ kevin [AT]scrye [DOT] com , bckurera [AT] fedoraproject
[DOT] org

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:Kevin[Kevin Fenzi] - Mentor, link:https://fedoraproject.org/wiki/User:Bckurera[Buddhike
Kurera] Co-mentor.

_Notes:_ This idea is improving, please contact for upto-date details.

[[design-hub-floss-collaboration-for-floss-designers]]
==== Design Hub: FLOSS Collaboration for FLOSS Designers

_Status:_ Proposed - Draft

_Summary of idea:_ Free software designers don't have a great set of
tools to work with to collaborate with each other and with the community
on their design work. With this project, we'd like to make progress
towards fixing that. We have some disparate ideas / tools that we'd like
to be integrated:

* http://sparkleshare.org[Sparkleshare] - a git-backed, Dropbox like
  system that will automatically check in and push files in project
  directly to a shared git repo
* https://github.com/garrett/magicmockup[Magic Mockup] - a
  coffeescript/javascript you can insert into an SVG of mockups to enable
  interactive, click-through mockups
  (http://blog.linuxgrrl.com/2011/08/12/interactive-svg-mockups-with-inkscape-javascript/[see
  a demo here]
* http://blog.linuxgrrl.com/category/design-hub/[Design Hub] - an idea
  and a ruby on rails prototype of a web front end that could potentially
  serve as a front end to git repos with design assets *and* serve as well
  as a front end to magic mockup mockups in said repos
* http://inkscape.org[Inkscape] is our preferred design tool of choice;
  it would be great if it had some GUI integration with Magic Mockup,
  Sparkleshare, and Design Hub

_Knowledge prerequisite:_ Ruby on Rails, web development, some UI design
chops

_Skill level:_ Intermediate

_Contacts:_ duffy at fedoraproject [dot] org

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:Duffy[ Mo Duffy], link:https://fedoraproject.org/wiki/User:Emichan[ Emily Dirsh]

_Notes:_ We can provide a development platform for the web UI at
openshift.redhat.com. Also, note this project doesn't just benefit
Fedora designers, but it benefits all FLOSS designers.

[[implement-a-unit-test-framework-for-fedpkg-and-rpkg]]
==== Implement a unit test framework for fedpkg and rpkg

_Status:_ Proposed

_Summary of idea:_ Unit tests are good. Testing is good. fedpkg and it's
backend rpkg is a growing code base, gaining more and more contributors.
It lacks a unit test framework which would be very beneficial to the
code base for making sure contributions don't break the code in subtle
ways.

_Knowledge prerequisite:_

_Skill level:_

_Contacts:_ jkeating [AT]redhat [DOT] com

_Mentor(s):_ User:jkeating - Mentor

_Notes:_ This project is in python.

[[insight-use-cases-for-calendar]]
==== Insight use cases for calendar

Status: Proposed - Draft

Insight project requirement to be implemented. More details can be found
at the wiki, Insight_use_cases_for_calendar

_Knowledge prerequisite:_ Knowledge on PHP and Drupal would be essential

_Skill level:_ Medium

Contacts: link:https://fedoraproject.org/wiki/User:Pfrields[Paul W. Frields], link:https://fedoraproject.org/wiki/User:Herlo[Clint Savage] &
link:https://fedoraproject.org/wiki/Insight[Insight Team]

Mentor(s): link:https://fedoraproject.org/wiki/User:Tatica[María "tatica"], link:https://fedoraproject.org/wiki/User:Asrob[Peter Tibor Borsa]
and _Back-up Mentor :_ link:https://fedoraproject.org/wiki/User:Bckurera[Buddhika Kurera]

Notes: -

[[insight-use-cases-for-status-and-microblogging]]
==== Insight use cases for status and microblogging

Status: Proposed - Draft

These are use cases for status and microblogging services that we might
want to provide through Insight. More details can be found at the wiki,
Insight_use_cases_for_status_and_microblogging

_Knowledge prerequisite:_ Knowledge on Drupal would be essential.

_Skill level:_ High

Contacts: link:https://fedoraproject.org/wiki/User:Pfrields[Paul W. Frields]

Mentor(s): link:https://fedoraproject.org/wiki/User:Asrob[Peter Tibor Borsa], link:https://fedoraproject.org/wiki/User:Tatica[María "tatica"]
and _Back-up Mentor :_ link:https://fedoraproject.org/wiki/User:Bckurera[Buddhika Kurera]

Notes: Students who are interested on this topic is highly requested to
contact the mentors as the first step. Knowledge on Drupal would be
essential.

[[insight-use-cases-for-events]]
==== Insight use cases for events

Status: Proposed

This integration should facilitate Fedora ambassadors to organize their
events easily and at the same time should be compatible with the Insight
calender. Please refer to link:https://fedoraproject.org/wiki/Insight_use_cases_for_events[Insight use
cases for events.]

_Knowledge prerequisite:_ Knowledge on Drupal would be essential. PHP
and MySQL

_Skill level:_ Medium

Mentor(s): link:https://fedoraproject.org/wiki/User:Bckurera[Buddhika Kurera], link:https://fedoraproject.org/wiki/User:Asrob[Peter Tibor Borsa]

[[isitfedoraruby.com]]
==== isitfedoraruby.com

_Status:_ Proposed

_Summary of idea:_ Right now most Ruby programmers make use of the
http://rubygems.org/[gem] package management system to install Ruby
libraries on their system. The link:https://fedoraproject.org/wiki/Ruby_SIG[Fedora/Ruby] community
works hard to convert these gems into rpms for inclusion in the Fedora
stack, making use of various tooling such as
https://github.com/lutter/gem2rpm[gem2rpm]. We are looking for more
tools and capabilities around Ruby / Fedora integration, namely to
reduce the overhead in supporting Ruby on Fedora and to promote Fedora
as the de-facto platform for Ruby development.

As part of this, we would like to develop a isitfedoraruby.com website
(similar to http://isitruby19.com/[isitruby19] and other sites like it)
to promote the ruby stack on Fedora and the Fedora/Ruby development
effort, highlighting success stories, use cases, ways contributors can
help, etc

_Knowledge prerequisite:_

_Skill level:_

_Contacts:_ link:https://fedoraproject.org/wiki/User:_Mmorsi[Mo Morsi]

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:_Mmorsi[Mo Morsi], link:https://fedoraproject.org/wiki/User:_Matthewwagner[ Matt Wagner]
as co-mentor

_Notes:_ Ping me (mmorsi on freenode) for more info. This project should
be written in Ruby

[[setup-gitlab-as-a-front-end-for-fedora-hosted-git-repositories]]
==== Setup Gitlab as a front end for Fedora Hosted git repositories

_Status:_ Proposed

_Summary of idea:_ To setup http://gitlabhq.com/[Gitlab] as a front end
for git repositories at [fedorahosted.org].

Git has fundamentally improved the way that developers share code. The
barrier to sharing code has virtually been eliminated. We are also
seeing the emergence of a new dynamic called "Social Coding". There's no
better example of this than Github.

Creating an environment to foster social coding, and a low barrier to
sharing code, requires more than just git, though. A lot of what makes
Github successful with git is the web-based front end. That explains why
there is such a dramatic difference between Github and fedorahosted.org,
and why many projects are moving to Github instead.

Fortunately, there is an open source application named Gitlab that
provides much of the interactive functionality and usability that is
found at Github. The goal of this project is to bring that experience to
fedorahosted.org by setting up Gitlab.

This project will require working with the infrastructure team to get
the necessary prerequisites installed, find a server to host the
application and configure the existing projects to be wired to this
interface.

_Knowledge prerequisite:_ Ruby, git, Linux system administration (web
servers, authentication)

_Skill level:_ Medium

_Contacts:_ link:https://fedoraproject.org/wiki/User:mojavelinux[Dan Allen] (general), link:https://fedoraproject.org/wiki/User:Vondruch[Vít
Ondruch] (packaging)

_Mentor(s):_ Ranjib Dey (tentative), Seth Vidal (tentative), Dmitriy
Zaporozhets (dzaporozhets), Ariejan de Vroom (ariejan)

_Notes:_ This idea is definitely something the Fedora project is
interested in pursuing. The work in this project will be carried on
after the project, and is an opportunity for a longer-term involvement
in Fedora.

See the discussion on the Fedora Infrastructure list about
implementation details:
http://lists.fedoraproject.org/pipermail/infrastructure/2012-March/011463.html

A demo of Gitlab can be found here: http://gitlabhq.com/demo.html

[[dorrie-a-web-interface-for-building-fedora-spinsremixes]]
==== Dorrie: A web-interface for building Fedora spins/remixes

_Status:_ Proposed

_Summary of idea:_ Contribute to Dorrie, and make it more usable, add
test coverage and deployable. Details at
link:https://fedoraproject.org/wiki/Remixes_Web_Interface[Dorrie].

_Knowledge prerequisite:_ Python, Django

_Skill level:_ Intermidiate

_Contacts:_ link:https://fedoraproject.org/wiki/User:Shreyankg[Shreyank Gupta]

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:Shreyankg[Shreyank Gupta], link:https://fedoraproject.org/wiki/User:Rtnpro[Ratnadeep
Debnath]

_Notes:_ Features that I am looking to target:

`* User management/FAS integration` +
`* Un-linerize the workflow.` +
`* Default wallpaper/Hostname/user-password/root-password` +
`* Custom repos/RPMs` +
`* Home directory content` +
`* Search Packages` +
`* Image type: ISO, virt image, raw disk`

Code at https://github.com/shreyankg/Dorrie, fork and send pull request,
if you are interested to contribute.

[[semi-automated-system-implementation-for-fwn]]
==== Semi-automated system implementation for FWN

_Status:_ Proposed

_Summary of idea:_ The idea behind this project is to make the
link:https://fedoraproject.org/wiki/FWN[Fedora Weekly News] composing less cumbersome and
semi-automated.

_Knowledge prerequisite:_ PHP, Python, Parsing, XML

_Skill level:_ Intermediate

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:Bckurera[Buddhike Kurera]

_Notes:_ Contact the mentor if anyone is interested.

[[linux-system-services]]
== Linux system services

[[improving-fedora-packaging]]
=== Improving Fedora packaging

[[java-apiabi-changes-checker]]
==== Java API/ABI changes checker

_Status:_ Proposed

_Summary of idea:_ Libraries written in Java add, remove and modify
their public interfaces from time to time. This is normal, but currently
it is very hard to guess effect an update of library to new version will
have on rest of the system. What is needed is a tool that would be able
to tell us that "With update of package java-library to version 2.0,
function X(b) has been removed. This function is used in package
java-app". There are already a few open-source projects that can do some
of the analysis needed. This would be of interest to whole Java world I
believe and would enable safer and easier updates. To get an idea of
similar projects see http://linuxtesting.org/upstream-tracker/java/[Java
API compliance checker] and
https://sites.google.com/site/obriencj/projects/java-classfile-python-module[Python
Javaclass project]

_Knowledge prerequisite:_ Knowledge of Java (inheritance rules,
generics, etc.) and probably some scripting language(s)

_Skill level:_ Fairly high

_Contacts:_ link:https://fedoraproject.org/wiki/User:Sochotni[Stanislav Ochotnický]

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:Sochotni[Stanislav Ochotnický]

_Notes:_ Don't hesitate to get in touch via IRC (sochotni@FreeNode) or
email (contact on my user page).

[[aur-clone-for-fedora]]
==== AUR Clone for Fedora

_Status:_ Proposed - Students' Idea - Need a mentor

_Summary of idea:_ The idea is to make an AUR clone for Fedora where
users can submit buildfiles, which need to be hosted. These set of files
can compile packages on the user side, and installed locally.

The benefit of this method is:

1.  No need to host the entire packages (saving space on servers hosting
    the RPMs)
2.  Make changes to the buildfile, and not compile the package
3.  Can pull sources from any version control system/stored tarballs to
    make a very upto-date package. (See blog entry for an example)

_Contacts:_ link:https://fedoraproject.org/wiki/User:Anujmore[Anuj More]

_Notes:_ Need mentor. Need advice on feasibility of the software and
issues that can arise. Also, read the extensive blog post on
http://execat.blogspot.in/2012/03/aur-clone-for-fedora.html[my blog].

Analogy: This is a lot like "having your own library" vs "getting a book
from a bookstore". If you refer a book often, you'd keep it in your
library. This would occupy space and consume resources. Otherwise, you'd
just pull out a separate paper with instructions about the bookstore to
find the book, and fetch the book from the store. Though the first
method is hassle-free, it is not possible to have all the books in the
world with you.

[[maven-foss-repository-extension]]
==== Maven FOSS Repository Extension

_Status:_ Proposed

_Summary of idea:_ With the inception of Maven we have a means to
standardize the building of Java projects. However it has also resulted
in the usage of a very wide dependency set by these Java projects. This
makes it very hard to integrate those projects on a single platform (be
it Fedora or JBoss Application Server (or the combination)).

In essence during packaging all these dependencies must be lined up to
form a single consistent component set. Thus each project will only have
those components available which are actually available on the target
platform. This is very hard to enforce using standard Maven setup.

For Fedora I'm envisioning a Maven extension that makes sure only
sanctioned components are used during the build (without putting a
burden on the packager). This will allow developers who are not on
Fedora to also build with this extension and thus verify (and fix)
issues which are the result of "Fedora packaging".

A prototype can be viewed at
https://github.com/wolfc/fedora-maven[Fedora Maven Extension].

_Knowledge prerequisite:_ Knowledge of Maven

_Skill level:_ Medium

_Contacts:_ http://community.jboss.org/people/wolfc[Carlo de Wolf]

_Mentor(s):_ http://community.jboss.org/people/wolfc[Carlo de Wolf]

_Notes:_ You can find me at irc.freenode.net #fedora-java wolfc

[[fedora-spins-and-remixes]]
=== Fedora Spins and remixes

[[fedora-audio-creation-spin]]
==== Fedora Audio Creation Spin

_Status:_ Proposed

_Summary of idea:_ To create a Fedora Audio spin showcasing the rich and
diverse landscape of Linux Audio production.

The link:https://fedoraproject.org/wiki/Audio_Creation[Fedora Audio Creation SIG] is a collection of
enthusiastic Fedora users driven by the common desire to make the best
Linux distribution also the best for Music creation and Audio
Production.

We are looking for one or two candidates to assist us in the complete
development cycle of this project. Tasks may include (but not limited
to):

* packaging open source audio projects
* porting certain packages from the PlanetCCRMA repository into Fedora
* working/communicating with the Fedora Audio community to determine the
  final make-up of the spin
* help coordinate pushing the spin through the Spins Review process
* creating/testing kickstart files
* developing small applications/scripts to help solve hardware and audio
  system configuration requirements
* communicating with Fedora Audio SIG, Fedora Desktop team and other
  Fedora teams
* organizing QA testcases and test days

Ideally, interested candidates will have a passion for Music/Audio
Production. Basic scripting knowledge and the ability to compile
projects from source is a must. Sponsored packagers / knowledge of the
Fedora Packaging Guidelines and/or the desire to continuing maintaining
the packaged software post-project considered a plus.

Interested? Why wait, submit an audio package for review and get
sponsored (Join_the_package_collection_maintainers).

_Knowledge prerequisite:_ Scripting/programming (bash/python)

_Skill level:_ Rudimentry

_Contacts:_ link:https://fedoraproject.org/wiki/User:bsjones[Brendan Jones]

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:bsjones[Brendan Jones] and link:https://fedoraproject.org/wiki/User:Crantila[Christopher
Antila]

[[fedora-jboss-spin]]
==== Fedora JBoss Spin

_Status:_ Proposed

_Summary of idea:_ To create a Fedora JBoss spin that helps Java
developers get started quickly using JBoss software.

The link:https://fedoraproject.org/wiki/SIGs/Java[Java SIG] and the link:https://fedoraproject.org/wiki/JBossAS7[JBoss AS 7 on Fedora
Initiative] is a cross-section of enthusiastic Fedora users driven by
the common desire to make the Fedora the best OS for developing with
JBoss software.

We are looking for one or two candidates to assist us in the complete
development cycle of this project. The bulk of the effort lies in
packaging open source Java and JBoss projects and integrating many of
the packages from the JBoss Community into Fedora. Packages in
particular include JBoss AS 7, JBoss Tools and JBoss Forge. Other tasks
may include the testing of kickstart files and developing configurations
to better integrate with the Fedora Desktop.

There could be two variants of this spin (feel free to choose). One is
on the developer needing a good desktop to create JBoss-based
applications (things like Eclipse + JBoss Tools, AS 7, Forge, a
database, and so on). The other focus is on getting a server setup so
that it can run the application. That would also be where the upcoming
OpenShift toolset will come into play.

Another critical component is the interoperability with Eclipse. It's
important that a setup for Eclipse gets augmented for a developer to
install/implement the JBoss specific tools and plugins. A decision needs
to be done, whether updates of Eclipse should be done via RPMs or via a
Eclipse repository. JBossAS7 is targeting a maven repository setup - one
option could simply be to tie into this repository.

Ideally, interested candidates will have a passion for Java development
using JBoss software. Basic scripting knowledge and the ability to
compile Java projects from source is a must. Sponsored packagers /
knowledge of the Fedora Packaging Guidelines and/or the desire to
continuing maintaining the packaged software post-project considered a
plus, but not required.

Interested? Why wait, submit a Java package for review and get sponsored
(link:https://fedoraproject.org/wiki/Join_the_package_collection_maintainers[Join the package
collection maintainers]).

_Knowledge prerequisite:_ Scripting/programming (Java, shell)

_Skill level:_ Rudimentary

_Contacts:_ link:https://fedoraproject.org/wiki/Goldmann[Marek Goldmann],
http://community.jboss.org/people/wolfc[Carlo de Wolf],
http://community.jboss.org/people/alrubinger[Andrew Rubinger],
https://community.jboss.org/people/maxandersen[Max Andersen],
link:https://fedoraproject.org/wiki/User:bit4man[Peter Larsen]

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:Goldmann[Marek Goldmann],
http://community.jboss.org/people/wolfc[Carlo de Wolf]

[[educational-application-for-fedora-robotics-suite]]
==== Educational Application for Fedora Robotics Suite

_Status:_ Proposed

_Summary of Idea:_ Create an educational app introducing software from
Fedora Robotics Suite

The link:https://fedoraproject.org/wiki/SIGs/Robotics[Fedora Robotics SIG] is creating a
link:https://fedoraproject.org/wiki/Features/RoboticsSuite[Robotics Suite] consisting of many packages
useful in robotics. We want to develop a demonstration application
introducing new users step by step to core packages like
http://www.fawkesrobotics.org[Fawkes] and
http://playerstage.sourceforge.net[Player/Stage]. It would feature
multiple game-like levels with increasing complexity. The general task
would be to instruct the robot to fulfill a specific task in a
simulation environment.

_Knowledge prerequisite:_ Developing this requires a strong background
in C++, a background in robotics is preferred but not necessary. You
should be able to familiarize yourself with new software quickly. User
visible parts will require GUI programming using Gtkmm.

_Skill level:_ Medium to High

_Contacts:_ link:https://fedoraproject.org/wiki/Timn[Tim Niemueller]

_Mentor(s):_ link:https://fedoraproject.org/wiki/Timn[Tim Niemueller]

[[applications-for-systems-administrators]]
=== Applications for systems administrators

[[rhq-agent-to-interface-with-matahari]]
==== RHQ-agent to interface with Matahari

_Status:_ Proposed

_Summary of idea:_ Write a RHQ-agent in Python and make it interface
with Matahari to pick up metrics that are provided by Matahari from
Fedora or RHEL systems. The agent would talk to the RHQ server via the
REST api and push metrics to RHQ etc. This agent will not implement the
full functionality of the RHQ java agent.

While RHQ is written in Java, this project does not need any Java
knowledge.

_Knowledge prerequisite:_ Python, Linux system administration, qpid,
principles of REST

_Skill level:_ Medium to High

_Contacts:_ Heiko Rupp

_Mentor(s):_ Heiko Rupp

_Notes:_ RHQ wiki is at http://rhq-project.org/

[[applications-for-testers]]
=== Applications for Testers

[[fedora-gooey-karma]]
==== Fedora Gooey Karma

_Status:_ Proposed

_Summary of idea:_ link:https://fedoraproject.org/wiki/Fedora_Easy_Karma[Fedora Easy Karma] is a CLI
application to help testers apply karma to installed updates and while
it's great at doing so, it doesn't display much other information on
what an update is supposed to fix or what can be done to test a
particular update. A GUI tool that pulls in more information on packages
under test would help in the testing process and hopefully help
encourage less techical users to start testing packages.

_Knowledge prerequisite:_ GUI tool kit (Qt or GTK), python

_Skill level:_ intermediate

_Contacts:_ link:https://fedoraproject.org/wiki/User:Tflink[Tim Flink (tflink)]

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:Tflink[Tim Flink (tflink)]

_Notes:_ A longer description and some rough mockups
http://blog.tirfa.com/gooey-karma[can be found here]

[[fedora-on-demand-build-service]]
==== Fedora On-Demand Build Service

_Status:_ Proposed (Student Idea)

_Summary of idea:_ During the testing of Fedora releases, test images
are often useful as smoke tests before full TC/RC composes, as baselines
for specific test days or for automated installation testing in AutoQA.
The idea is to make an *on-demand* Web-based build service (similar to
https://build.opensuse.org/[Open Suse Build Service] and
http://www.slax.org/build.php[Slax]) which users/developers can use to
make custom Fedora based distributions. The service would be capable of
building and hosting images (boot iso, installation DVDs and live
images) made up of builds from stable repositories in addition to side
repos containing specific builds from both updates-testing and koji
builds that have yet to be pushed to any repos. The service should also
also have a RESTful API (or similar).

_Knowledge prerequisite:_ Python, Fedora repositories

_Knowledge nice-to-have_: Fedora image building, testing, Celery

_Skill level:_ intermediate to advanced

_Contacts:_ link:https://fedoraproject.org/wiki/User:Amitksaha[Amit Saha]

_Mentor(s):_ link:https://fedoraproject.org/wiki/User:Tflink[Tim Flink (tflink)]

_Notes:_ Looking for co-mentors, will update this idea if any are found

