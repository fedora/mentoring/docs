[[google-summer-of-code-2025]]
= Google Summer of Code 2025


NOTE: Fedora is proud to have been accepted as a GSoC mentoring organization.  Student applications open on March 24, 2025 1800UTC.  Please make sure you carefully read through the xref:gsoc/2025/index.adoc[general information] and xref:gsoc/2025/application.adoc[application process] pages before applying.

image::gsoc_logo.png[float="right"]

This page contains information about Fedora's participation in Google
Summer of Code (GSoC).  Please feel free to contact us via the mailing list below
for clarifications and more information. You can also use the IRC channel.

[[what-can-i-do-today]]
== What can I do today?


Fedora is still in the process of applying to be a mentoring organization.
Today there is nothing we can do except wait until Google makes a
decision.

////
Fedora is proud to have been accepted as a GSoC mentoring organization.
Student applications open on XYZ.
////

Today you should read through the xref:gsoc/2025/ideas.adoc[ideas] and think about
our xref:gsoc/2025/application.adoc[application process.]

[[student-information]]
== Student Information

Do you want to contribute to one of the world's leading innovative Linux
distributions? GSoC could be your chance. Please refer to the information
below.

* If you're interested in working on a mentor-submitted idea, read
  the xref:gsoc/2025/ideas.adoc[ideas page] and about the project and the related
  technology. There is no need to contact the mentor unless you have a
  specific question about the project. Don't send an "I'm interested" email.

* You may also wish to start working on the Fedora specific parts of
  the application, see below for more information. Carefully review the
  student responsibilities section.

[[why-spend-your-summer-working-on-foss]]
=== Why spend your summer working on FOSS?

When you work in the open on free software, you create a body of work
that follows you for the rest of your life. Rather than a coding
assignment done by thousands of other students and relegated to the
bottom of the drawer at semester's end, working in FOSS is a chance to
contribute to a living project.

Working in FOSS gives you a chance to:

* Work with real-world, large codebases.
* Collaborate with real engineers and other professionals.
* Contribute to something meaningful while learning and earning.
* Learn tools and processes that are just like what you are going to use
  if you work in technology after graduation.
* Make friends and contacts around the globe.
* Attract attention that can lead to an internship or job after
  graduation.
* Create lifetime connections and associations.

[[why-work-with-fedora]]
=== Why work with Fedora?

Our project is large and diverse. We are very experienced at working
with new contributors and helping them be successful.

Many of our contributors are long-time contributors. They remain because
they want to keep growing the project and to lend their expertise,
advice and mentoring to you! People who stay around the community and do
good work are noticed. They get hired for jobs from it, including being
hired by Red Hat. Past Google Summer of Code students were hired by Red
Hat, as well as interns in various positions. This is just an example,
as experience and reputation in the Fedora Project communities is
influential in your career in many ways.

As a long-standing community with many facets, it is possible for you to
find many rewarding sub-projects to work on.

You should know that contributing to FOSS doesn't require you to have
super programming skills, or super-anything else. You just need be
interested, curious, and willing to become comfortable being
productively lost. This is the state of learning. You learn by finding
your way around and figuring things out with the support of your mentor
and the community.

If you are new to the Fedora Project, the following material will help
you to get started.

* link:https://docs.fedoraproject.org/fedora-project/project/fedora-overview.html[Fedora Project Overview]
* link:https://docs.fedoraproject.org[Fedora Technical and Community Documentation]
* link:https://fedoraproject.org/wiki/How_to_use_IRC[How to use IRC]
* link:https://fedoraproject.org/wiki/Development[Information about Development in Fedora]

[[student-responsibilities]]
=== Student Responsibilities

You are the key ingredient for your project's success. This project is
important to you, your mentor, and the entire Fedora Community.

Your responsibilities include:

* Communicating early and often with your mentor.
* Blogging every week about what you're learning, how you're doing,
  challenges and successes. This is key way to keep the entire Fedora
  Community informed.
* Working with your mentor on realistic achievable milestones that
  provide for regular deliverables and feedback.
* Attending the brief student calls, as announced
* Being accountable for your success and your actions

[[student-application]]
=== Student Application

Please read and follow the
xref:gsoc/2025/application.adoc[student application process].

[[mentor-information]]
== Mentor Information

Want to help the next generation of contributors grow? Want to bring new
contributors to Fedora? Want to advance your projects along their
roadmap? GSoC can be a chance to do all of this.

NOTE: If you are contacted directly by a student, we encourage you
to reply and include the mailing list and/or IRC channel to keep the
process transparent and to ensure the inquiry is able to be answered by
more people than just you.

=== How to Propose a Project

If you want to mentor a specific project, think carefully about several things:

1. Do you have enough time to work on this with the student during the
   entire project.  You will be helping someone else when they get stuck.
   You don't want to become a blocker because you're busy.

2. It is harder to find success when you are completely certain of how an
   idea needs to be implemented; finding a student with the skills and
   interest to implement a specific solution is a lot harder than finding
   a student with enough skills to respond to a use case need. Also,
   students learn more when they help design and guide the project. In
   other words, provide guidance and direction but let the student do
   some of the "driving."

3. Where you can have looser ideas, you may be able to find a student
   who works as a sort-of intern who can implement a solution to a use
   case you have. In past experiences, students going after a use case are
   more likely to get somewhere with self-direction and support from you.

4. Who can help you?  Try to find a second mentor for the project.

If you're interested in working with a student on
a specific project you should post your idea to the
https://pagure.io/mentored-projects/issues[Mentored Projects Issue
Tracker].  Your issue should be tagged *GSoC* and use the *Google Summer
of Code* template.  We strongly encourage you to find a second person
to help with mentoring and to solicit feedback on your proposal

=== Can I be a Mentor Without a Project?

Yes!  You can either:

* Work with a student who brings an idea to your sub-project. This
  requires a different level of communication throughout the project,
  but can be the most rewarding.

* Be a general mentor.  This is a person who works with all students
  regardless of their project.  To become a general mentor please open
  an issue in the https://gitlab.com/fedora/mentoring/home[Mentored
  Projects Issue Tracker] offering your help.  Please tag the issue with
  the *GSoC* tag.

[[how-to-work-with-students]]
=== How to work with students

* Read about good mentoring in the
  link:http://write.flossmanuals.net/gsoc-mentoring/what-makes-a-good-mentor[Manual
  on Mentoring]

[[mentor-responsibilities]]
Mentor responsibilities
^^^^^^^^^^^^^^^^^^^^^^^

You are an essential part of the student's success, the project's
success, and the success for the Fedora Project.

Your responsibilities include:

* Being an interface for an identified sub-project or SIG in Fedora.
* Helping students communicate with the overall project and any
  upstream.
* Helping the student plan realistic achievable milestones that provide
  for regular deliverables and feedback.
* Regular communication with your student. This means you must be
  regularly available for the entire project. If you take a holiday you
  need to know early and help your student be ready for your brief
  absence.
* Attending a brief mentors call, as scheduled.
* Be the final, accountable person for deciding if the student is
  successful or not. This is responsibility akin to being a professor or
  boss.

[[communication]]
== Communication

* *Mailing List (GSoC-related):* link:https://lists.fedoraproject.org/admin/lists/summer-coding@lists.fedoraproject.org/[summer-coding@lists.fedoraproject.org]
* *Mailing List (Technical):* link:https://lists.fedoraproject.org/admin/lists/devel@lists.fedoraproject.org/[devel@lists.fedoraproject.org]
* *Matrix/element Chatroom:* link:https://chat.fedoraproject.org/#/room/#google-summer-coding:fedora.im[#google-summer-coding]

[[timeline]]
== Timeline

Always refer to the
link:https://summerofcode.withgoogle.com/how-it-works/#timeline[Official
Google Summer of Code Timeline] for details.

[[administration]]
Administration
~~~~~~~~~~~~~~

In order to get questions answered or obtain more information related to
this year's GSoC with Fedora, please contact the administrators directly
(please consider CCing the summer-coding list where ever possible).

1. link:https://fedoraproject.org/wiki/User:Sumantrom[Sumantro Mukherjee] 
* Email : sumantro@redhat.com
2. link:https://fedoraproject.org/wiki/User:ffmancera[Fernando F. Mancera]
* Email : ferferna@redhat.com 
