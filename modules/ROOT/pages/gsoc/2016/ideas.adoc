Find an idea you like? Want to propose your own? See the
link:https://fedoraproject.org/wiki/GSOC_Guide_studentsGSoC[Getting Started Guide].

[[students-welcome]]
== Students Welcome

If you are a student looking forward to participate the GSoC 2016 with
Fedora, please feel free to browse the idea list which is still growing.
Do not hesitate to contact the mentors or contributors as indicated in
this page for any related clarification. You also should find some
like-minded people on the `#fedora-summer-coding` IRC channel.

If you are new to the Fedora Project, the following material will help
you to get started. Additionally, please register in the link:https://fedoraproject.org/wiki/Account_System?rd=FAS[Fedora
Account System (FAS)] if you are willing to continue with the Fedora
Project. For getting quick help, `#fedora-devel` can be used for getting
help with programming problems.

1.  link:https://docs.fedoraproject.org/fedora-project/project/fedora-overview.html[The Four Foundations of Fedora]
2.  link:https://developers.google.com/open-source/gsoc/resources/[Official
    GSoC Resources]
3.  link:http://docs.fedoraproject.org/en-US/index.html[Fedora Documentation]
4.  link:https://fedoraproject.org/wiki/How_to_use_IRC?rd=Communicate/IRCHowTo[IRC]
5.  link:https://fedoraproject.org/wiki/Development[Development]

[[supporting-mentors]]
== Supporting Mentors

The following contributors are available to provide general help and
support for the GSoC 2016 program (existing contributors, feel free to
add yourselves and your wiki page). If a specific project mentor is
busy, you can contact one of the people below for short-term help on
your project or task.

1.  link:https://fedoraproject.org/wiki/User:Decause[Remy DeCausemaker]
2.  link:https://fedoraproject.org/wiki/User:Kushal[Kushal Das]
3.  link:https://fedoraproject.org/wiki/User:Hguemar[Haïkel Guémar]
4.  link:https://fedoraproject.org/wiki/User:Jberkus[Josh Berkus]
5.  link:https://fedoraproject.org/wiki/User:Lmacken[Luke Macken]
6.  link:https://fedoraproject.org/wiki/User:Lsd[Lali Devamanthri]

[[fedora-atomic-mentors]]
=== Fedora Atomic Mentors

The following mentors are specifically available for projects related to
link:http://www.projectatomic.io/community/gsoc[Fedora Atomic]:

1.  Giuseppe Scriviano
2.  link:https://fedoraproject.org/wiki/User:Walters[Colin Walters]
3.  link:https://fedoraproject.org/wiki/User:Gscrivano[Giuseppe Scrivano]
4.  link:https://fedoraproject.org/wiki/User:Mbarnes[Matthew Barnes]

[[idea-list-for-gsoc-2016]]
== Idea list for GSoC 2016

[[implement-tinykdump]]
=== Implement Tinykdump

_Status_: Proposed - draft

_Summary of idea_: Tinykdump is a minimal daemon to capture kernel-based
crash dumping (kdump) memory image to usb storage. Compared to the
traditional kdump solution, it is,

`* more reliable and scalable` +
`* has smaller memory foot-print` +
`* more friendly to kernel developers `

More information here: https://fedorahosted.org/tinykdump/

_Knowledge prerequisite_: Python, kernel programming (desired)

_Skill level_: intermediate (programming)

_Contacts_: link:https://fedoraproject.org/wiki/User:caiqian[CAI Qian]

_Mentor(s)_: link:https://fedoraproject.org/wiki/User:caiqian[CAI Qian]

_Notes_: Rough roadmap:

* Implement tinykdump daemon to be included in Fedora.
* Submit kernel patches for reserving kdump memory at run-time for
  community review and inclusion.
* Currently, pstore only log kernel messages for panic and Oops. Patches
  are needed to support logging of kdump kernel and initramfs console
  output.

[[implement-mips-bits-to-packages]]
=== Implement MIPS bits to packages

_Status_: Proposed

_Summary of idea_: Some packages are missing implementation bits for
MIPS architecture. The goal of this project is implementing and posting
them upstream.

_URL_: https://fedoraproject.org/wiki/Architectures/MIPS

_Knowledge prerequisite_: Packaging, Python, C

_Skill level_: intermediate

_Contacts_: link:https://fedoraproject.org/wiki/User:mtoman[Michal Toman]

_Mentor(s)_: link:https://fedoraproject.org/wiki/User:mtoman[Michal Toman]

_Notes_: The following packages are in question:

* *java* - builds on 32-bit MIPS but does not work, dies with SIGILL
* *redhat-lsb* - misses assembly bits for MIPS
* *anaconda/blivet* - completely MIPS unaware
* *valgrind* - no FPXX support on 32-bit MIPS, builds on mips64el but
  does not work
* *elfutils* - unwinding is not supported on MIPS
* *u-boot* - build working MIPS images
* ... there are more but these are the most painful

This is obviously too much for a single project, we are going to agree
on a reasonable subset.

_Expected outcome_:

* Better MIPS support in upstream projects and Fedora packages
* Learn about MIPS architecture
* Learn to communicate with upstreams

[[idea-list-for-fedora-community-operations-and-infrastructure]]
== Idea list
for Fedora Community Operations and Infrastructure

[[fedora-infrastructure-web-application-development-community-operations-commops]]
=== Fedora Infrastructure Web Application Development: *Community Operations
(CommOps)*



[cols="3,10",options="header",]
|=======================================================================
|Project |*Community Operations* (CommOps) 

|IRC Channel |#fedora-commops
|Web page |link:https://fedoraproject.org/wiki/CommOps[CommOps Wiki] 
|Mentor(s)|* link:https://fedoraproject.org/wiki/User:decause[ Remy DeCausemaker] (decause)

* link:https://fedoraproject.org/wiki/USER:corey84[ Corey Sheldon] (linux-modder)

| Notes
|
The rise of DevOps has been swift. Sysadmins are increasingly
instrumenting and integrating automated systems to stand up and maintain
their infrastructure. This same approach can be taken to support
community infrastructure in a distributed and automated fashion, that
doesn't force people to choose between using their precious volunteer
time to "build things" or "build communities that build things." The
community operations team works across numerous technologies, and
interest areas in Fedora, including Messaging, Storytelling, Fedora
Badges, Fedora Hubs, Wiki, Culture, Metrics, Voting, and other
miscellaneous topics. It is important to note that CommOps team members
are generalists, and the strengths and interests of individual
applicants will determine the precise nature of your work. Enthusiasm
for FOSS culture and community organization, and effective communication
skills are most important. Specific deliverables can include automated
metrics gathering, real-time messaging integration with Fedora hubs, and
other big-data analysis and visualization. Tools are primarily written
in python, and utilize libraries such as fedmsg, numpy, pandas, pygals,
flask, sqlalchemy. We also leverage other web technologies like HTML5
and Javascript, and frameworks such as bootstrap and handlebars to
deliver these metrics.

Your internship with this project could also involve any or all of the
following:

* Web testing and bug reporting / triaging
* Content development and syndication
* Writing for the web and print
* Collaboration with Fedora Council and Leadership on Project Objectives and Initiatives
* Volunteer Coordination and Organizing
* Campaign development and implementation
* Advocacy and Messaging

Required:

* Effective Communication Skills; written and verbal, synchronous and asynchronous.
* Passion for Free/Open Source Software and Free Culture
* Self-directed and Curious nature
* Experience writing for the web (Content only is ok, HTML/CSS even better)
* Experience publishing and/or syndicating content via Social Media
* Experience working in teams, or community organizing
* Basic Web development/design skills

Bonus Skills:

* Basic multimedia development skills
* Sysadmin/Devops skills
* programming/scripting skills

|=======================================================================

[[fedora-infrastructure-web-application-development-fedora-hubs]]
=== Fedora Infrastructure Web Application Development: *Fedora Hubs*

[cols="3,10",options="header",]
|=======================================================================
|Project |Fedora Infrastructure Web Application Development: *Fedora Hubs*
|IRC Channel |#fedora-hubs

|Web page |
* link:https://fedoraproject.org/wiki/Infrastructure[ Fedora Infrastructure Team]
* link:https://fedoraproject.org/wiki/Fedora_Hubs[Fedora Hubs Wiki]

|Mentor(s) |

* link:https://fedoraproject.org/wiki/User:lmacken[ Luke Macken] (lmacken)
* link:https://fedoraproject.org/wiki/User:decause[ Remy DeCausemaker] (decause)
* link:https://fedoraproject.org/wiki/USER:corey84[ Corey Sheldon] (linux-modder)

 
| Notes
|
Fedora has been in the process of creating a new web presence for all
of the Fedora users, subprojects, and communities. We also plan to
integrate some of the features of this system with the
https://developer-phracek.rhcloud.com[prototype] of the Fedora Developer
Portal, which is targeted at helping general developers use Fedora.
While the Hubs project itself is aimed at Fedora contributor circles,
the reusable features will allow the Portal to use this work to speak to
general developers who may not have an interest in working in the Fedora
community, but want to connect with community members for advice, best
practices, or to generate interest in _their_ upstream project.

Several of the principal app developers in the Fedora Engineering team
will be working together with you on this project during the GSoC term.
You'll participate directly with team members daily on core features of
Hubs as well as integration with the Portal. This is a great opportunity
both for regular mentorship and for being deeply involved in an exciting
and fast-moving project with the Fedora team.

Working on this project could involve any of the following:

* Adding new features to the web frontend
* Adding new capabilities to the backend
* Writing and deploying new widgets
* Triaging and processing new widget ideas submitted by the community at large.
* Implementing existing (and thoroughly detailed) mockups from previous UI/UX interns and team members.

Skills:

* HTML/Javascript/CSS
* Basic Python a plus
* Bonus: Experience with Bootstrap, jinja templates, Fedmsg, datanommer/grepper, or other fedora infrastructure projects a HUGE plus.

When applying to this project, it may be useful to review the designers'
blogs about the projects' design here:

Fedora Developer Portal

* http://blog.linuxgrrl.com/2015/09/15/fedora-developer-website-design/

Fedora Hubs

* http://blog.linuxgrrl.com/category/fedora-hubs
* https://meghanrichardson.wordpress.com/

A video overview of the Hubs project is available here:
http://blog.linuxgrrl.com/2015/07/01/fedora-hubs-update/

|=======================================================================

[[fedora-infrastructure-web-application-development-pagure]]
=== Fedora Infrastructure Web Application Development: *Pagure*

[cols="3,10",options="header",]
|=======================================================================
|Project |*Pagure* 
|IRC Channel |#fedora-apps 
|Web page |https://pagure.io[Pagure project] 
|Mentor(s) |link:https://fedoraproject.org/wiki/User:pingou[
Pierre-Yves Chibon] (pingou) 
| Notes
|
Pagure is a forge written in python and offering the possibility of
self-hosting projects while still using the now well-know fork and
pull-request contribution model. While being similar to other forge such
as GitHub or GitLab it is also pretty different for example it does not
namespace projects under usernames with the idea that projects should
not belong to a single person but to a community.

Your internship with this project could also involve any or all of the
following:

* Web testing and bug reporting / triaging
* Content development and syndication
* Participating in the effort to make of pagure the official front-end to Fedora's packages git repositories (currently being the read-only cgit)
* Implementing private projects in pagure
* Figuring out how/if pagure can be a front-end for Fedora's git repo now that they are namespaced (to offer more than just rpms, for example docker containers)

Required:

* Effective Communication Skills; written and verbal, synchronous and asynchronous.
* Passion for Free/Open Source Software and Free Culture
* Self-directed and Curious nature
* Experience with Flask, javascript and HTML
* Experience with git
* Sysadmin/Devops skills

Bonus Skills:

* Previous experience with Pagure
* Experience with developing APIs and CLIs using it

|=======================================================================

[[idea-list-for-fedora-atomic-gsoc-2016]]
== Idea list for Fedora Atomic GSoC 2016

This idea list comes from the
http://www.projectatomic.io/community/gsoc[Project Atomic] website. The
most up-to-date version is there.

[[next-generation-super-privileged-container]]
=== Next-generation Super Privileged Container

[cols="3,10",options="header",]
|=======================================================================
|Project |_Fedora Atomic_ 
|IRC Channel |#atomic 
|Web page |https://projectatomic.io[ProjectAtomic.io] 
|Mentor(s) |

* link:https://fedoraproject.org/wiki/User:walters[ Colin Walters] (walters)
* link:https://fedoraproject.org/wiki/User:gscrivano[ Giuseppe Scrivano] (gscrivano)

 
|Notes |

* https://github.com/projectatomic/atomic/issues/298[Next generation
super-privileged containers]: Improve building, managing, and updating
these container images]

Difficulty: Advanced

Required skills:

* C programming
* Python programming
* Docker and/or runC experience

Bonus Skills:

* Golang programming
* SELinux knowledge

Expected outcomes

* Implement ability for user to install flannel, etcd, and other bootstrap containers and manage them easily
* New super-priv containers are accepted into planned design of Atomic Host
* Learn how container security works
* Learn advanced SELinux hackery
* Learn how to manage container dependances

|=======================================================================

[[atomic-host-package-layering]]
=== Atomic Host package layering

[cols="3,10",options="header",]
|=======================================================================
|Project |_Fedora Atomic_ 
|IRC Channel |#atomic 
|Web page |https://projectatomic.io[ProjectAtomic.io] 
|Mentor(s) |

* link:https://fedoraproject.org/wiki/User:walters[ Colin Walters] (walters)
* link:https://fedoraproject.org/wiki/User:gscrivano[ Giuseppe Scrivano] (gscrivano)

 
|Notes |

* https://github.com/projectatomic/rpm-ostree/pull/107[Atomic Host package layering]: Improve the package layering design, support more RPMs, ensure %post scripts are safe, etc.]

Required:

* C programming
* Fedora or other Linux

Bonus Skills:

* Knowledge of RPM packaging
* Experience working in Linux distributions

Libraries and Software:

* rpm-ostree, libhif

Difficulty Level:

* Advanced

Expected outcomes

* Ability to layer debugging, bootstrap, and other packages
* Learn how OStree images work
* Learn packaging security practices and theory

|=======================================================================

[[bootstrap-with-gpgcheck-in-kickstart]]
=== Bootstrap with gpgcheck in kickstart

[cols="3,10",options="header",]
|=======================================================================
|Project |_Fedora Atomic_ 
|IRC Channel |#atomic 
|Web page |https://projectatomic.io[ProjectAtomic.io] 
|Mentor(s) |

* link:https://fedoraproject.org/wiki/User:jberkus[ Josh Berkus] (jberkus)
* link:https://fedoraproject.org/wiki/User:gscrivano[ Giuseppe Scrivano] (gscrivano)
* link:https://fedoraproject.org/wiki/User:corey84[ Corey Sheldon] (linux-modder)

 
|Notes |

* https://github.com/projectatomic/rpm-ostree/issues/190[Bootstrap with gpgcheck in kickstart]: Add a way for importing a GPG key from the kickstart `ostreesetup` command before the download starts.
  https://docs.fedoraproject.org/en-US/Fedora/23/html/Installation_Guide/appe-kickstart-syntax-reference.html["ostreesetup" is described here]

Required:

* Ability to use kickstart images
* Experience with virt helpful
* Fedora or other Linux Experience

Libraries and Software:

* kickstart
* gpg
* virt
* RPM-OStree

Difficulty Level:

* Intermediate

Expected outcomes

* Ability to kickstart atomic images with full gpg support working
* Improved software supply chain hygiene for project atomic
* Learning how secure provisioning works
* Learning how to get code merged upstream

|=======================================================================

[[improve-ability-to-monitor-runningcanceled-transactions]]
=== Improve ability to monitor running/canceled transactions

[cols="3,10",options="header",]
|=======================================================================
|Project |_Fedora Atomic_ 
IRC Channel |#atomic 
|Web page |https://projectatomic.io[ProjectAtomic.io] 
|Mentor(s) |

* link:https://fedoraproject.org/wiki/User:jberkus[ Josh Berkus] (jberkus)
* link:https://fedoraproject.org/wiki/User:gscrivano[ Giuseppe Scrivano] (gscrivano)

 
| Notes |

* https://github.com/projectatomic/rpm-ostree/issues/210[Improve ability to monitor running/canceled transactions]: The rpm-ostree client termination doesn't block the command execution on the rpm-ostreed.
  This will cause new rpm-ostree clients to fail immediately because there is a transaction in progress. Change rpm-ostree to be notified of the status of the current transaction and possibly attach to it.

Required:

* C programming experience
* Ability to create Virtual Machines (VMs) or have access to spare PC nearby for testing.
* Fedora or other Linux Experience

Bonus Skills:

* Experience with RPM packaging

Libraries and Software:

* rpm-ostree

Difficulty Level:

* Intermediate

Expected outcomes

* Ability for project to better understand transactions happening in the deamon
* Ability for project to re-attach and get transaction status
* Understand RPM-OSTree upgrade model and how tree transactions work
* Learn how to work with upstreams

|=======================================================================

[[support-for-end-of-life-notification]]
=== Support for end-of-life notification

[cols="3,10",options="header",]
|=======================================================================
|Project |_Fedora Atomic_ 
|IRC Channel |#atomic 
|Web page |https://projectatomic.io[ProjectAtomic.io]
|Mentor(s) |link:https://fedoraproject.org/wiki/User:jberkus[ Josh Berkus] (jberkus) 
|Notes |

* https://github.com/projectatomic/rpm-ostree/issues/142[Support for end-of-life notification]: Add support for having an `end-of-life` notification to inform users if a particular branch is not supported anymore.

Required:

* C programming experience
* Fedora or other GNU/Linux Experience

Bonus Skills:

* Advanced C programming experience
* experience with RPM packaging

Libraries and Software:

* rpm-ostree
* atomic CLI

Difficulty Level:

* Novice

Expected outcomes

* rpm-ostree and atomic command output explaining when a version of an upgraded tree branch is end of life (EoL)
* feature merged into future releases, and packaged for distribution
* Learn how to work with upstreams and Linux distributions
* Understand how EOL policies and lifecycles work

|=======================================================================

[[rpm-ostree-operation-history-support]]
=== rpm-ostree operation history support

[cols="3,10",options="header",]
|=======================================================================
|Project |*Fedora Atomic* 
|IRC Channel |#atomic 
|Web page |https://projectatomic.io[ProjectAtomic.io]
|Mentor(s) |link:https://fedoraproject.org/wiki/User:jberkus[ Josh Berkus] (jberkus) 
|Notes |

* https://github.com/projectatomic/rpm-ostree/issues/85[rpm-ostree operation history support]: Add support for `atomic history` to display the transactions history.
  It should work in a similar way to `yum history`.

Required:

* C programming experience
* Python Programming experience
* Fedora or other GNU/Linux experience

Bonus Skills:

* Advanced C programming experience
* Advanced Python programming experience
* Ability to work with Upstreams

Difficulty Level:

* Intermediate

Expected outcomes

* Duplication of support for history in the atomic command, similar to "yum history"
* Strong command history and output on the system, possibly in systemd journal

|=======================================================================

[[support-metalink-for-ostree]]
=== Support Metalink for OSTree

[cols="3,10",options="header",]
|=======================================================================
|Project |*Fedora Atomic* 
|IRC Channel |#atomic 
|Web page |https://projectatomic.io[ProjectAtomic.io] 
|Mentor(s) |

* link:https://fedoraproject.org/wiki/User:jberkus[ Josh Berkus] (jberkus)
* link:https://fedoraproject.org/wiki/User:gscrivano[ Giuseppe Scrivano] (giuseppe)

 
|Notes |

* https://bugzilla.gnome.org/show_bug.cgi?id=729388[Support metalink for OSTree]: Add support for metalink files and support downloads from a list of mirrors and fetch objects from multiple sources.

Required:

* C programming experience
* Metalink experience
* Experience working with RPM packaging

Bonus Skills:

* Advanced C programming experience
* Ability to work with Upstreams

Difficulty Level:

* Intermediate

Expected outcomes

* Ability to use a list of mirrors for OSTree upgrade
* Ability to fetch trees in parallel

|=======================================================================

[[drop-privileges-for-http-fetches]]
=== Drop privileges for HTTP fetches

[cols="3,10",options="header",]
|=======================================================================
|Project |_'Fedora Atomic_ 
|IRC Channel |#atomic 
|Web page |https://projectatomic.io[ProjectAtomic.io]
|Mentor(s)|

* link:https://fedoraproject.org/wiki/User:jberkus[ Josh Berkus] (jberkus)
* link:https://fedoraproject.org/wiki/User:gscrivano[ Giuseppe Scrivano] (giuseppe)

 
|Notes |

* https://bugzilla.gnome.org/show_bug.cgi?id=730037[Drop privileges for HTTP fetches]: The HTTP fetcher code is running in the same process of OSTree.
  Move the HTTP fetcher code to another process with less privileges than the main process.

Required:

* C programming experience
* SELinux knowledge

Bonus Skills:

* Advanced C programming experience
* libsoup experience helpful
* Advanced SELinux Experience
* Ability to work with Upstreams

Difficulty Level:

* Intermediate

Expected outcomes

* OSTree uses a different process for fetching data over HTTP
* Main OSTree process communicates through Inter Process Communication (IPC) with fetcher process
* Fetcher confined to write-only in a temporary file

|=======================================================================

[[support-kpatch]]
=== Support kpatch

[cols="3,10",options="header",]
|=======================================================================
|Project |_Fedora Atomic_ 
|IRC Channel |#atomic 
|Web page |https://projectatomic.io[ProjectAtomic.io] 
|Mentor(s) |

* link:https://fedoraproject.org/wiki/User:jberkus[ Josh Berkus] (jberkus)
* link:https://fedoraproject.org/wiki/User:gscrivano[ Giuseppe Scrivano] (giuseppe)
* link:https://fedoraproject.org/wiki/User:corey84[ Corey Sheldon] (linux-modder)

 
|Notes |

* https://github.com/projectatomic/rpm-ostree/issues/118[Support kpatch]: Support live update for the kernel without rebooting or restarting any processes.

Required:

* C programming experience
* Python programming experience
* Basic knowledge of D-Bus

Bonus Skills:

* Advanced C programming experience
* Advanced Python programming experience
* Advanced knowledge of D-Bus
* Familiarity with kpatch

Difficulty Level:

* Intermediate

Expected outcomes

* Partial live update feature integrated into Fedora Atomic Host
* Learn to implement live kernel update
* Learn to work with upstream project integration
* Understand RPM-OSTree packaging and images

|=======================================================================

[[automatic-atomic-host-updates]]
=== Automatic Atomic Host Updates

[cols="3,10",options="header",]
|=======================================================================
|Project |_Fedora Atomic_ 
|IRC Channel |#atomic 
|Web page |https://projectatomic.io[ProjectAtomic.io] 
|Mentor(s) |

* link:https://fedoraproject.org/wiki/User:walters[ Colin Walters] (walters)
* link:https://fedoraproject.org/wiki/User:gscrivano[ Giuseppe Scrivano] (giuseppe)

 
|Notes |

* https://github.com/projectatomic/rpm-ostree/issues/177: implement a service that automatically upgrades the system when a new image is available.
  If the system is not restarting correctly, the rollback to the previous working version.

Required:

* C programming experience
* Python programming experience
* Basic knowledge of systemd
* Ability to create and run VMs.

Bonus Skills:

* Advanced C programming experience
* Advanced Python programming experience
* Familiarity with RPM packaging

Difficulty Level:

* Intermediate

Expected outcomes

* Automated updates integrated into Fedora Atomic Host
* Learn how RPM-OStree packaging and images work
* Develop ability to contribute to Atomic Host

|=======================================================================

[[idea-list-for-cockpit]]
== Idea list for Cockpit

[[cockpit-support-for-systemd-timers]]
=== Cockpit support for systemd timers

[cols="3,10",options="header",]
|=======================================================================
|Project |_Cockpit_ 
|IRC Channel |#cockpit 
|Webpage |http://cockpit-project.org 
|Mentor(s) |

* Dominik Perpeet (dperpeet)
* Peter Volpe (petervo)

 
|Notes |

* Systemd provides timers for calendar time events and monotonic time events (http://www.freedesktop.org/software/systemd/man/systemd.timer.html, https://wiki.archlinux.org/index.php/Systemd/Timers).
  A major component of the Fedora Server is the Cockpit Project, a web-based management console for servers.

* Some designs for timers in Cockpit exist at https://trello.com/c/1B2lZViZ/74-timers-and-cron.

Required:

* JavaScript (ideally jQuery)
* Fedora or other Linux

Bonus Skills:

* Familiarity with D-BUS
* Familiarity with Python (2.7)
* Experience working in Linux distributions

Libraries and Software:

* cockpit (http://cockpit-project.org/)

Difficulty Level:

* Beginner to intermediate

Expected outcomes

* A user of the Cockpit UI is able to view existing timers, edit existing ones or create new timers while providing the minimum set of necessary information to the UI
* The UI optionally allows more advanced settings to be selected
* Functionality of added features is tested via unit and/or integration tests

|=======================================================================

[[ostree-repositories-in-cockpit]]
=== OStree Repositories in Cockpit

[cols="3,10",options="header",]
|=======================================================================
|Project |_Cockpit_ 
|IRC Channel |#cockpit 
|Web page |http://cockpit-project.org
|Mentor(s) |

* Dominik Perpeet (dperpeet)
* Peter Volpe (petervo)


|Notes |

* Cockpit provides an interface for updating the installed software on Atomic systems using rpm-ostree.
  Admins should be able to examine the OSTree repos enabled on a machine, and add/edit/remove them via Cockpit.

Required:

* JavaScript (ideally angular)
* Fedora or other Linux

Bonus Skills:

* Familiarity with D-BUS
* Familiarity with Python (2.7)
* Familiarity with OSTree
* Experience working in Linux distributions

Libraries and Software:

* cockpit (http://cockpit-project.org/)
* rpm-ostree (https://github.com/projectatomic/rpm-ostree)

Difficulty Level:

* Beginner to intermediate

Expected outcomes

* A user of the Cockpit UI is able to examine the OSTree repos enabled on a machine, and add/edit/remove them.
* Functionality of added features is tested via unit and/or integration tests

|=======================================================================

[[ostree-rebases-in-cockpit]]
=== OStree Rebases in Cockpit

[cols="3,10",options="header",]
|=======================================================================
|Project |_Cockpit_ 
|IRC Channels |#cockpit 
|Webpage |http://cockpit-project.org 
|Mentor(s) |

* Dominik Perpeet (dperpeet)
* Peter Volpe (petervo)


|Notes |

* Cockpit provides an interface for updating the installed software on Atomic systems using rpm-ostree.
  Admins should be able to 'rebase' to a different operating system channel or timeline via Cockpit.

Required:

* JavaScript (ideally angular)
* Fedora or other Linux

Bonus Skills:

* Familiarity with D-BUS
* Familiarity with Python (2.7)
* Familiarity with OSTree
* Experience working in Linux distributions

Libraries and Software:

* cockpit (http://cockpit-project.org/)
* rpm-ostree (https://github.com/projectatomic/rpm-ostree)

Difficulty Level:

* Beginner to intermediate

Expected outcomes

* A user of the Cockpit UI is able to be able to 'rebase' to a different operating system channel or timeline via Cockpit.
* Functionality of added features is tested via unit and/or integration tests

|=======================================================================

[[setup-a-freeipa-server-in-cockpit-using-rolekit]]
=== Setup a FreeIPA server in Cockpit using Rolekit

[cols="3,10",options="header",]
|=======================================================================
|Project |_Cockpit_ 
|IRC Channel |#cockpit 
|Webpage |http://cockpit-project.org 
|Mentor(s) |

* Dominik Perpeet (dperpeet)
* Peter Volpe (petervo)

 
|Notes |

* FreeIPA is a domain and directory server. This task implements a UI for setting it up.

* The Rolekit Project provides a platform API for deploying Server Roles such as FreeIPA onto a system.
  Currently, it supports creating a Domain Controller (based on FreeIPA) or a Database Server (based on PostgreSQL).
  A major component of the Fedora Server is the Cockpit Project, a web-based management console for servers.
  The goal of this effort would be to enhance the Cockpit UI so that an administrator could deploy the FreeIPA role.

* Some designs for rolekit integration into Cockpit exist at
  https://trello.com/c/7CZqL9AQ/54-rolekit-integration-for-domain-controller.

Required:

* JavaScript (ideally jQuery)
* Fedora or other Linux

Bonus Skills:

* Familiarity with D-BUS
* Familiarity with Python (2.7)
* Experience working in Linux distributions
* Domain or directory experience

Libraries and Software:

* cockpit (http://cockpit-project.org/)
* FreeIPA (https://www.freeipa.org/page/Main_Page)
* Rolekit (https://fedorahosted.org/rolekit/)

Difficulty Level:

* Intermediate

Expected outcomes

* A user of the Cockpit UI is able to deploy a Domain Controller while providing the minimum set of necessary information to the UI
* The UI allows more advanced settings to be selected
* The UI also provides a link post-deployment that allows the user to browse to the Domain Controller administration UI

|=======================================================================

[[docs]]
== Docs

[[anerist]]
=== Anerist

[cols="3,10",options="header",]
|=======================================================================
|Project |Docs Project 
|IRC Channel |#fedora-docs
|Webpage |https://fedoraproject.org/wiki/Docs_Project 
|Mentor(s) |link:https://fedoraproject.org/wiki/User:zoglesby[ Zach
Oglesby] (zoglesby) 
|Notes |
The Fedora community produces a variety of content suitable for user
reference. Members of the Fedora Project produce documentation in a
variety of ways - manpages and code comments from developers, system
architecture design and best practices reference material from the infra
team; application design, interface design, microservices, buildfarms
and more. All at work meeting the needs of a transparent organization
built on open source technologies and open development practices.

Anerist is intended to consume reference materials, in whatever format
they are discovered in, and render them to produce a neatly indexed html
site with a common style and theme. To alleviate manual effort in this
conversion process, and in the process of 'cycling' translations, the
application is event driven, with content regenerated on each change of
the sources using CICD technologies. This design relies on modularized
conversion tools, which also allows for programmatically generated
content and drop-in custom views of technologies or infrastructure.

We need your help to build this framework. You'll use python, bootstrap,
buildbot, fedmsg, git, and more.

Your internship with this project could also involve any or all of the
following:

* Exposure to content development workflows.
* Mentoring from seasoned sysadmins, veteran technical writers, and free software advocates.
* Learning the Open Source Way, the practice of transparent positive collaboration that forms bedrock of Fedora's Foundations.
* 

Required:

* general python skills
* experience with generating and rendering ReStructuredText
* familiarity with DocBook, or other structured contextual XML.
* familiarity with Jenkins, buildbot, or similar buildsystems

Bonus Skills:

* UX Design experience
* Free software advocacy
* Extended community participation
* Ownership of amusing hats

|=======================================================================

[[open-ideas-from-gsoc-2015]]
== Open Ideas From GSoC 2015

In addition to the above list of ideas, you may want to check out ideas
from previous years and contact the mentors for those projects to see if
they're still interested in mentoring someone this year.

_Note_: Do not submit a proposal for an idea from a previous year
without contacting the mentor to ensure they will be available to mentor
you. *Without a mentor, proposals will be rejected.*
////
Previous years:

* link:Summer_coding_ideas_for_2015[2015]
* link:Summer_coding_ideas_for_2014[2014]
* link:Summer_coding_ideas_for_2013[2013]
* link:Summer_coding_ideas_for_2012[2012]
* link:Summer_coding_ideas_for_2011[2011]
* link:Summer_coding_ideas_for_2010[2010]
* link:Summer_coding_ideas_for_2009[2009]
* link:Summer_coding_ideas_for_2008[2008]

Category:Summer_coding[Category:Summer coding]
Category:Summer_coding_2016[Category:Summer coding 2016]
////
