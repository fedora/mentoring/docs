[[google-summer-of-code-2018]]
= Google Summer of Code 2017


image:GSoC_2017.png[]


This wiki page serves as the GSoC portal. Please feel free to contact us
via list for clarifications and more information. You can also use the
IRC channel, .

[[what-can-i-do-today]]
== What can I do today?

Fedora has been accepted as a mentoring organization. Student
applications open on 20 March.

* Students
** If you're interested in working on an existing project, read up on
   the project and the related technology. There is no need to contact the
   mentor unless you have a specific question about the project. Don't send
   "I'm interested" email.
** If you're interested in proposing your own project start looking
   around the Fedora Project for a mentor and send your idea to the mailing
   list or post it on the wiki.

Students may also wish to start working on the Fedora specific parts of
the application (see the application section)

* Mentors

If you're interested in working with a student you should post your idea
to the idea page.

[[students]]
== Students

Do you want to contribute to one of the world's leading and innovative
Linux distributions? GSoC could be your chance. Please refer to the
material below and start contacting mentors.

* xref:ideas.adoc[Ideas]

[[why-spend-your-summer-working-on-foss]]
=== Why spend your summer working on FOSS?

When you work in the open on free software, you create a body of work
that follows you for the rest of your life. Rather than a coding
assignment done by thousands of other students and relegated to the
bottom of the drawer at semester's end, working in FOSS is a chance to
contribute to a living project.

Working in FOSS gives you a chance to:

* Work with real-world, large codebases.
* Collaborate with real engineers and other professionals.
* Contribute to something meaningful while learning and earning.
* Learn tools and processes that are just like what you are going to use
  if you work in technology after graduation.
* Make friends and contacts around the globe.
* Attract attention that can lead to an internship or job after
  graduation.
* Create lifetime connections and associations.

[[why-work-with-fedora]]
=== Why work with Fedora?

Our project is large and diverse. We are very experienced at working
with new contributors and helping them be successful.

Many of our contributors are long-time contributors. They remain because
they want to keep growing the project and to lend their expertise,
advice and mentoring to you! People who stay around the community and do
good work are noticed. They get hired for jobs from it, including being
hired by Red Hat. Past Google Summer of Code students were hired by Red
Hat, as well as interns in various positions. This is just an example,
as experience and reputation in the Fedora Project communities is
influential in your career in many ways.

As a long-standing community with many facets, it is possible for you to
find many rewarding sub-projects to work on.

You should know that contributing to FOSS doesn't require you to have
super programming skills, or super-anything else. You just need be
interested, curious, and willing to become comfortable being
productively lost. This is the state of learning. You learn by finding
your way around and figuring things out with the support of your mentor
and the community.

[[student-responsibilities]]
=== Student Responsibilities

You are the key ingredient for your project's success. This project is
important to you, your mentor, and the entire Fedora Community.

Your responsibilities include:

* Communicating early and often with your mentor.
* Blogging every week about what you're learning, how you're doing,
  challenges and successes. This is key way to keep the entire Fedora
  Community informed.
* Working with your mentor on realistic achievable milestones that
  provide for regular deliverables and feedback.
* Attending a brief monthly student call
* Being accountable for your success and your actions

[[student-application]]
=== Student Application

Please read and follow the
xref:application_process.adoc[student application process].

[[mentors]]
== Mentors

Want to help the next generation of contributors grow? Want to bring new
contributors to Fedora? Want to advance your projects along their
roadmap? GSoC can be a chance to do all of this.

The contributors of the Fedora Project can propose ideas and mentor
them. Please feel free to check following links and please add your
ideas to the main idea page. Furthermore, if you are not interested in
proposing an idea but still want to support the program, please check
the students' idea page and pick one as per your interest. Lastly,
consider becoming a Supporting Mentor and helping students across
projects.

1.  link:http://write.flossmanuals.net/gsoc-mentoring/what-makes-a-good-mentor[Manual
    on Mentoring]
2.  xref:ideas.adoc[Main Idea Page and Supporting
    Mentor List]

[[how-to-work-with-students]]
=== How to work with students

* One way is to provide an idea for students to work on. This idea might
  be very well planned out, in which case you may need a high-level of
  contact with the student to get it implemented correctly. Other ideas
  may be more general and require more planning during onboarding.

* It is harder to find success when you are completely certain of how an
  idea needs to be implemented; finding a student with the skills and
  interest to implement a specific solution is a lot harder than finding a
  student with enough skills to respond to a use case need. Also, students
  learn more when they help design and guide the project. In other words,
  provide guidance and direction but let the student do some of the
  "driving."

* Where you can have looser ideas, you may be able to find a student who
  works as a sort-of intern who can implement a solution to a use case you
  have. In past experiences, students going after a use case are more
  likely to get somewhere with self-direction and support from you.

* You may also want to work with a student who brings an idea to your
  sub-project. This requires a different level of communication throughout
  the project, but can be the most rewarding.

[[mentor-responsibilities]]
=== Mentor responsibilities

You are an essential part of the student's success, the project's
success, and the success for the Fedora Project.

Your responsibilities include:

* Being an interface for an identified sub-project or SIG in Fedora.
* Helping students communicate with the overall project and any
  upstream.
* Helping the student plan realistic achievable milestones that provide
  for regular deliverables and feedback.
* Regular communication with your student. This means you must be
  regularly available for the entire project. If you take a holiday you
  need to know early and help your student be ready for your brief
  absence.
* Attend a brief monthly mentors call.
* Be the final, accountable person for deciding if the student is
  successful or not. This is responsibility akin to being a professor or
  boss.

[[list-of-mentors]]
=== List of Mentors

Mentors are listed on the xref:ideas.adoc[Main Idea Page and Supporting Mentor List]

[[communication]]
== Communication

* *Mailing List (GSoC-related) :*  link:https://lists.fedoraproject.org/admin/lists/summer-coding@lists.fedoraproject.org/[summer-coding@lists.fedoraproject.org]
* *Mailing List (Technical) :*  link:https://lists.fedoraproject.org/admin/lists/devel@lists.fedoraproject.org/[devel@lists.fedoraproject.org]
* *IRC :* link:https://webchat.freenode.net/?channels=#fedora-summer-coding[#fedora-summer-coding] or link:https://webchat.freenode.net/?channels=#fedora-devel[#fedora-devel] on Freenode



[[timeline-abbreviated]]
== Timeline (Abbreviated)

* Reference:
  https://developers.google.com/open-source/gsoc/timeline[Full Timeline]

* *10 October, 2016*: Program announced.
* *19 January, 2017: 16:00 UTC* Mentoring organizations can begin
  submitting applications to Google.
* *09 February: 16:00 UTC* Mentoring organization application deadline.
* *10 - 26 February*: Google program administrators review organization
  applications.
* *27 February 16:00 UTC* List of accepted mentoring organizations
  published on the Google Summer of Code site.

Interim Period: Would-be students discuss project ideas with potential
mentoring organizations.

* *20 March: 16:00 UTC* Student application period opens.
* *03 April: 16:00 UTC* Student application deadline.

Interim Period: Slot allocation trades happen among organizations.
Mentoring organizations review and rank student proposals; where
necessary, mentoring organizations may request further proposal detail
from the student applicant.

* *04 May: 16:00 UTC* Accepted student proposals announced.

http://googlesummerofcode.blogspot.cz/2007/04/so-what-is-this-community-bonding-all.html[Community
Bonding Period] - Students get to know mentors, read documentation, get
up to speed to begin working on their projects.

* *30 May*: Students begin coding for their Google Summer of Code
projects; Google begins issuing initial student payments provided tax
forms are on file and students are in good standing with their
communities.

Work Period: Mentors give students a helping hand and guidance on their
projects.

* *26 June: 16:00 UTC* Mentors and students can begin submitting Phase 1
  evaluations.
* *30 June: 16:00 UTC* Phase 1 evaluations deadline; Google begins
  issuing mid-term student payments provided passing student survey is on
  file.

Work Period: Mentors give students a helping hand and guidance on their
projects.

* *28 July: 16:00 UTC* Phase 2 evaluations deadline.
* *21 to 29 August: 16:00 UTC*: Final week: Students tidy code, write
  tests, improve documentation and submit their code sample. Students also
  submit their final mentor evaluation.
* *29 August*: Mentors can start submitting final student evaluations.
* *05 September: 16:00 UTC*: Final evaluation deadline
* *06 September*: Final results of Google Summer of Code 2017 announced
* *Late October*: Mentor Summit at Google. Mentors and Organization
  Administrators from each participating organization are invited to
  Google for an unconference to collaborate on ideas to make the program
  better and to make new friends too!

[[ideas-page]]
== Ideas Page

* *Status :* Open for Ideas +
* *Link :* xref:ideas.adoc[Summer coding ideas for 2017]

[[links]]
== Links

1.  link:https://docs.fedoraproject.org/fedora-project/project/fedora-overview.html[The Four Foundations of Fedora]
2.  link:https://developers.google.com/open-source/gsoc/resources/[Official
    GSoC Resources]
3.  link:http://docs.fedoraproject.org/en-US/index.html[Fedora Documentation]
4.  link:https://fedoraproject.org/wiki/Communicate/IRCHowTo[IRC]
5.  link:https://fedoraproject.org/wiki/Development[Development]

[[administration]]
== Administration

In order to get questions answered or obtain more information related to
this year's GSoC with Fedora, please contact the administrators directly
(please consider CCing the summer-coding list where ever possible).

1.  link:https://fedoraproject.org/wiki/User:Bex[Brian (bex) Exelbierd] (Primary)
2.  link:https://fedoraproject.org/wiki/User:Spot[Tom Calloway] (Secondary)


