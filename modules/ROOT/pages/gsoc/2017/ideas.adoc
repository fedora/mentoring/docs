Find an idea you like? Want to propose your own? See the
xref:application_process.adoc[student application process].

[[students-welcome]]
== Students Welcome

If you are a student looking forward to participating in
link:index.html[GSOC 2017] with Fedora, please feel free to browse this
idea list which is still growing.

////
I don't know if this does work: (it's pink)
////

*Now please go read the link:https://fedoraproject.org/wiki/GSOC_2017#What_can_I_do_today.3F[What Can
I do Today] section of the main page.*

Do not hesitate to contact the mentors or contributors listed on this
page for any questions or clarification. You can find helpful people on
the IRC channel, or use the mailing list. can be used for getting help
with programming problems.

If you are new to the Fedora Project, the following material will help
you to get started. You should also follow the
xref:application_process.adoc[student application process]

* link:https://docs.fedoraproject.org/fedora-project/project/fedora-overview.html[The Four Foundations of Fedora]
* link:https://developers.google.com/open-source/gsoc/resources/[Official
  GSoC Resources]
* link:http://docs.fedoraproject.org/en-US/index.html[Fedora Documentation]
* link:https://fedoraproject.org/wiki/How_to_use_IRC?rd=Communicate/IRCHowTo[IRC]
* link:https://fedoraproject.org/wiki/Development[Development]

[[supporting-mentors]]
=== Supporting Mentors

The following contributors are available to provide general help and
support for the GSoC 2017 program (existing contributors, feel free to
add yourselves and your wiki page). If a specific project mentor is
busy, you can contact one of the people below for short-term help on
your project or task.

* link:https://fedoraproject.org/wiki/User:Bex[Brian (bex) Exelbierd] (Fedora Community Action and Impact
  Coordinator, FCAIC, 🎂, containers, general development, general Linux)
* link:https://fedoraproject.org/wiki/User:Jflory7[Justin W. Flory] (General development, general Linux,
  Fedora community, GSoC alumnus, questions about program, misc. advice)
* link:https://fedoraproject.org/wiki/User:Rhea[Radka (rhea) Janek] (C#, webserver or dotnet related stuff
  on Linux, general support and help with the program)
* link:https://fedoraproject.org/wiki/User:Linuxmodder[Corey Sheldon] (Python, 2Factor/Multi-Factor Auth, QA
  Testing, general mentoring, security, 2nd year mentor)
* link:https://fedoraproject.org/wiki/User:Skamath[Sachin S. Kamath] (General Linux, Fedora community,
  Metrics, GSoC alumnus, Help with program)
* link:https://fedoraproject.org/wiki/User:Bee2502[Bhagyashree(Bee)] ( Fedora community, Metrics, Python,
  past GSoC mentor, Support related to GSoC)
* link:https://fedoraproject.org/wiki/User:Lsd[Lali Devamanthri] (General development,
  Middleware/Integration, general Linux, Fedora community, GSoC alumnus,
  past GSoC mentor)
* link:https://fedoraproject.org/wiki/User:Devyani7[Devyani Kota] (Fedora community, GSoC Alumnus, General
  Linux, Python, questions related to GSoC)

[[draft-of-an-idea]]
=== Draft of an idea

Please add your idea using the following template. The template contains
_comments in italic text_, examples and questions that should be
answered. *Please copy the template (your idea) into the list of ideas -
do not change it here.*

[[project-name]]
==== Project Name

[cols="3,10",options="header",]
|=======================================================================
|Status |Proposed - draft _Use this status._
|*Skill level* |Novice / Intermediate / Proficient _Are the required
skills below something a beginner would no or could reasonably learn
quickly? Is there an area where knowledge is already expected making
this an advanced project? Also consider how much knowledge about Fedora
is required._

|*Skills required* |_Programming languages or other skills that the
student should already posess. Keep in mind that students come to both
practice thieir existing skills and grow. Scope your tasks for someone
to be able to apply and learn during the project, therefore you
shouldn't list everything required to complete the task._

|*Mentor(s)* |_If your SIG is taking the responsibility, specify as in
this example (and always link to people or groups)_
link:https://fedoraproject.org/wiki/SIGs/DotNet[DotNet SIG] - link:https://fedoraproject.org/wiki/User:Rhea[Radka (rhea) Janek], ...

|*Contacts (IRC & email)* |_link:https://webchat.freenode.net/?channels=#example-irc-channel[#example-irc-channel] & link:https://lists.fedoraproject.org/admin/lists/example-list@lists.fedoraproject.org/[exaple-list@lists.fedoraproject.org] - Mentors email or mailing list of your
SIG._

|*Idea description* |_Something something._

|*Notes & references* |_Something or nothing._
|=======================================================================

[[idea-list-for-gsoc-2017]]
== Idea list for GSoC 2017

[[directory-server-developing-administrative-tools]]
=== 389 Directory Server: developing administrative tools

[cols="3,10",options="header",]
|=======================================================================
|*Status* |Proposed - draft
|*Skill level* |Intermediate

|*Skills required* |Python: Must understand Classes, Inheritance, and
Modules

|*Mentor(s)* |link:https://fedoraproject.org/wiki/User:Firstyear[William Brown] (firstyear UTC+10, please
be patient!)

|*Contacts (IRC & email)* |link:https://webchat.freenode.net/?channels=#389[#839] & link:https://lists.fedoraproject.org/admin/lists/389-devel@lists.fedoraproject.org/[389-devel@lists.fedoraproject.org]

|*Idea description* a|
389 Directory Server is an enterprise class LDAP server, used in
businesses globally to authenticate and identify people. We have a large
code base that has gone through nearly 20 years of evolution.

Part of this evolution has been the recent addition of a python
administration framework designed to replace our legacy perl tools. The
framework already has the base classes designed and written, but we need
help to knit together the high level administrative functionality.

Throughout this process you will need to:

* Learn to deploy a 389 Directory Server.
* Learn some of the functions of 389 DS (account policy, plugin management).
* Read and interpret some of our existing perl and shell scripts.
* Extend the python tools dsconf to support enabling / disabling / configuration of modules in Directory Server to replace our legacy tools.
* Review other team members' python code.
* Participate in our community.

From this you will learn:

* How to integrate and use existing python frameworks and servers.
* Techniques to unit test command line and python tools.
* How to work with a geographically distributed team.
* Engineering principles expected of a project with high quality demands.
* Use of git and ticket trackers for a project.
* How to contribute to mailing lists and the review process.

What are we looking for:

* To teach you good community engagement, and engineering skills.
  The coding project is a means to help us teach you to interact effectively with a team, and to learn engineering principles.

Is this project right for you?

* Come and talk to wibrown on #389 in irc on freenode, or email our mailing list 389-devel@lists.fedoraproject.org

|*Notes & references* |link:http://www.port389.org/[port389.org]
|=======================================================================

[[asp.net-core-web-application-for-rfedora-subreddit]]
=== ASP.NET Core web application for /r/Fedora subreddit

[cols="3,10",options="header",]
|=======================================================================
|Status |Proposed - draft
|*Skill level* |Novice

|*Skills required* |Basic C#

|*Mentor(s)* |link:https://fedoraproject.org/wiki/SIGs/DotNet[DotNet SIG] - link:https://fedoraproject.org/wiki/User:Rhea[Radka (rhea)
Janek]

|*Contacts (IRC & email)* a|link:https://webchat.freenode.net/?channels=#fedora-dotnet[#fedora-dotnet]
& link:https://lists.fedoraproject.org/admin/lists/dotnet-sig@lists.fedoraproject.org/[dotnet-sig@lists.fedoraproject.org], radka.janek@redhat.com

|*Idea description* a|
Goals:

* ASP.NET Core web application for our link:https://www.reddit.com/r/Fedora[/r/Fedora subreddit] to let the user choose a flair, based on their FAS Group memberships, written in C# and deployed on a Fedora production server as systemd service.

You will learn:

* What is it .NET Core and how to use it in Linux
* How to write C# code on Linux and what IDEs are available to you.
* You will create and deploy an ASP.NET Core application on our Fedora production server as systemd service, with secure Apache in front of it.

|*Notes & references* |link:https://fedoraproject.org/wiki/DotNet[.NET on Fedora],
link:https://fedorahosted.org/ipsilon[ipsilon] ; _Due to high interest we
recommend that you apply for different project._
|=======================================================================

[[net-core-wrapper-library-for-systemd]]
=== .NET Core wrapper library for systemd

[cols="3,10",options="header",]
|=======================================================================
|Status |Proposed - draft
|*Skill level* |Intermediate

|*Skills required* |C# and basic Linux

|*Mentor(s)* |link:https://fedoraproject.org/wiki/SIGs/DotNet[DotNet SIG] - https://fedoraproject.org/wiki/User:Rhea[Radka (rhea)
Janek]

|*Contacts (IRC & email)* a|link:https://webchat.freenode.net/?channels=#fedora-dotnet[#fedora-dotnet]
& link:https://lists.fedoraproject.org/admin/lists/dotnet-sig@lists.fedoraproject.org/[dotnet-sig@lists.fedoraproject.org], radka.janek@redhat.com

|*Idea description* a|
Goals:

* .NET Standard library wrapping systemd, written in C# and published on NuGet.
* By utilizing systemd, we can control various aspects of the system underneath directly from C# (eg. stopping or starting services, scheduling reboots)

You will learn:

* What is it .NET Core and how to use it in Linux
* How to write C# code on Linux and what IDEs are available to you.
* How to create and maintain NuGet packages.
* How to utilize systemd and dbus to control your Linux system.

|*Notes & references* |link:https://fedoraproject.org/wiki/DotNet[.NET on Fedora] ; _Due to high
interest we recommend that you apply for different project._
|=======================================================================

[[continuous-static-analysis-db]]
=== Continuous static analysis db

[cols="3,10",options="header",]
|=======================================================================
|Status |Proposed - draft
|*Skill level* |Intermediate

|*Skills required* a|
* Python
* Distributed systems

|*Mentor(s)* |link:https://fedoraproject.org/wiki/User:Athoscr[ Athos Ribiero] (athoscr)

|*Contacts (IRC & email)* |IRC: athos Email: athoscribeiro@gmail.com

|*Idea description* |This project proposes the design and implementation
of a system to continuously run multiple security oriented static
analyzers on source code and display the alarms related to a specific
version of the analyzed software. The alarms to be presented will be
ranked based on their importance, where critical flaws shall be ranked
first and potential false positives are ranked last. We will develop a
tool to perform continuous static analysis with different static
analyzers and propose a warning classification method using their
outputs. We will also propose a visualization approach for the
information generated with our tool.

|*Notes & references* |link:https://fedoraproject.org/wiki/StaticAnalysis[Static Analysis SIG]
|=======================================================================

[[migrate-plinth-to-fedora-server]]
=== Migrate Plinth to Fedora Server

[cols="3,10",options="header",]
|=======================================================================
|Status |Proposed - draft
|*Skill level* |Novice

|*Skills required* a|
* Python and Django
* Git
* dnf knowledge
* ARM based hardware is a plus
* RPM packaging knowledge is a plus

|*Mentor(s)* |link:https://fedoraproject.org/wiki/User:Tonghuix[Tong Hui] (first year, UTC+8, please be
patient!)

|*Contacts (IRC & email)* a|
* link:https://webchat.freenode.net/?channels=#fedora-arm[#fedora-arm] & link:https://lists.fedoraproject.org/admin/lists/arm@lists.fedoraproject.org/[arm@lists.fedoraproject.org]

|*Idea description* a|
Plinth is developed by link:https://wiki.debian.org/FreedomBox/[Freedombox]
which is a Debian based project. The Freedombox is aiming for building a
100% free software self-hosting web server to deploy social applications
on small machines. It provides online communication tools respecting
user privacy and data ownership, replacing services provided by
third-parties that under surveillance.
link:https://wiki.debian.org/FreedomBox/Plinth[Plinth] is the front-end of
Freedombox, written in Python.

This idea mainly about migrate Plinth from Deb-based to RPM-based, and
make it available for Fedora Server which will running on ARM machines.
It would be better of student to be familiar with ARM based hardware and
knowing something about how to running a Fedora Server on it, or use
something like Qemu if you don't know hardware.

The main goal of this idea is to make Plinth works fine in Fedora Server
or Minimal flavor, due to Plinth write APT commands hard coded, so it is
better yo make it more adoptive for RPM. The secondary goal is to make a
RPM package for Plinth from source and setup a repo for it, so that
everyone who use Fedora could use Plinth.

You will learn:

* how to write modern test driven Python projects
* how to running Fedora Server on a ARM machine.
* how to migrate deb package to RPM package from source.

|*Notes & references* |link:https://fedoraproject.org/wiki/Architectures/ARM[Architectures/ARM]
|=======================================================================

[[patternfly-frontend-pattern-development]]
=== Patternfly Frontend Pattern Development

[cols="3,10",options="header",]
|=======================================================================
|Status |Proposed - draft
|*Skill level* |Intermediate

|*Skills required* |Javascript

|*Mentor(s)* |Brian Leathem

|*Contacts (IRC & email)* a|
link:https://webchat.freenode.net/?channels=#patternfly[#patternfly] & patternfly@redhat.com

|*Idea description* |Explore the emerging web component specifications
by implementing patternfly.org patterns as web components. Begin with
smaller simpler web component implementations to learn the APIs, then
follow on with a more complex composite component to fully exercise the
APIs and make a robust and feature-rich web components. Engage the
PatternFly developer community to learn and contribute to the collective
web component effort.
////
don't know if the link is working
////
|*Notes & references* |link:https://patternfly-webcomponents.github.io/
|=======================================================================

[[fedora-media-writer---new-features]]
=== Fedora Media Writer - New Features

[cols="3,10",options="header",]
|=======================================================================
|Status |Proposed - draft
|*Skill level* |Intermediate

|*Skills required* |C++/Qt and/or QML

|*Mentor(s)* |link:https://fedoraproject.org/wiki/User:Mbriza[Martin Bříza]

|*Contacts (IRC & email)* |mbriza on Freenode (for example on link:https://webchat.freenode.net/?channels=#fedora-devel[#fedora-devel] or link:https://webchat.freenode.net/?channels=#fedora-apps[#fedora-apps] ) or
mbriza@redhat.com

|*Idea description* a|
Fedora Media Writer is a tool that makes putting Fedora images on
portable media (such as flash drives) much easier. Since Fedora 25, it
is offered as the default download options for users coming to
link:https://getfedora.org[Get Fedora] from a Mac or a Windows computer.

Ever since the rewrite from liveusb-creator, there has been some
features missing from it and due to the popular demand, it wouldn't be a
bad idea to get them readded. You won't need to implement all of these.
Just one would be perfectly fine. However, please contact me first so we
can arrange the details of your potential application.

There is a short list of some things that could be done (however, feel
free to get in touch about anything else, or look on the GitHub page for
some more):

* _Persistent storage_.
  To explain how it works, I'll start with how live media works now for us: After booting into the live system, you can do whatever you want, install programs, save files to your home folder or even modify the root filesystem, as much as your free memory permits you.
  However, after rebooting you lose all those changes because the partitions on the drive are not touched. Persistent storage adds the possibility to retain those changes through reboots so you would be able to install new apps to your live environment or work with files stored on the flash drive.

* Being able to write the images without destroying the data that was already present on the flash drive before.

* Having a Fedora ARM image resized to fit the size of your SD card immediately after having it written.

* Proper privilege escalation through launchd for the helper process on macOS.

Understanding of libraries and techniques relevant to this project on
Linux, Mac or Windows (or all of them) is a big plus.

You'll learn more about multiplatform development and be involved in the
project that is the first thing new users see when they are going to try
Fedora.

|*Notes & references* |link:https://github.com/MartinBriza/MediaWriter
|=======================================================================

[[adwaita-and-highcontrast-style-for-qtquickcontrols]]
=== Adwaita and HighContrast style for QtQuickControls

[cols="3,10",options="header",]
|=======================================================================
|Status |Proposed - draft
|*Skill level* |Novice

|*Skills required* |QML and a little bit of C++

|*Mentor(s)* |User:Mbriza[Martin Bříza]

|*Contacts (IRC & email)* |mbriza on Freenode (for example on link:https://webchat.freenode.net/?channels=#fedora-devel[#fedora-devel] or link:https://webchat.freenode.net/?channels=#fedora-apps[#fedora-apps] ) or
mbriza@redhat.com
    

|*Idea description* a|
Fedora Workstation ships custom Qt themes that match its Adwaita and
Highcontrast themes from its default GNOME desktop environment. This
makes Qt applications blend nicely into the overall Fedora Worstation
experience.

However, to fully support most of the Qt applications, we need to have
QtQuickControls (QML) themes implemented, too.

Work on this would consist of three main parts:

* Figuring out where and how to install the theme (CMake)
* Implementing the look of all necessary widgets (QML)
* Making this theme selected by default when the app runs in GNOME (probably C++, in the QGnomePlatform project)

This project is best suited to a candidate who wants to learn about QML.

|*Notes & references* a|
link:https://github.com/MartinBriza/adwaita-qt

link:https://github.com/MartinBriza/highcontrast-qt

link:https://github.com/MartinBriza/QGnomePlatform

|=======================================================================

[[fedora-commops-centralized-metrics-generation]]
=== Fedora CommOps : Centralized Metrics generation

[cols="3,10",options="header",]
|=======================================================================
|*Status* |Proposed - draft
|*Skill level* |Intermediate

|*Skills required* a|
Required:

* Python
* Basic understanding of APIs
* Flask

Bonus Skills:

* Data analytics algorithms
* Knowledge of Fedora Apps

|*Mentor(s)* |link:https://fedoraproject.org/wiki/User:Skamath[ Sachin S. Kamath] (skamath) link:https://fedoraproject.org/wiki/User:Bee2502[
Bhagyashree(Bee) ] (bee2502)

|*Contacts (IRC & email)* a|link:https://webchat.freenode.net/?channels=#fedora-commops[#fedora-commops]
&
link:https://lists.fedoraproject.org/admin/lists/commops.lists.fedoraproject.org/[commops@lists.fedoraproject.org]

|*Idea description* a|
Right now, metrics collection in CommOps is not very efficient and
requires a lot of manual work. Metrics for various events/FAS
groups/users are collected using scripts which query datagrepper and
return results. This process is very time consuming and writing scripts
each time is a very tedious process. Also, querying the datagrepper to
get data everytime is redundant and time-consuming.

Hack on statscache to build a central metrics generation system for
Fedora with handy features to pull statistics. Take a look at the
link:https://pagure.io/fedora-commops/issue/105[Pagure ticket] for more
details.

|*Notes & references* |link:https://pagure.io/fedora-commops/issue/105[Pagure
ticket]
|=======================================================================

[[open-ideas-from-gsoc-2016]]
=== Open Ideas From GSoC 2016

In addition to the above list of ideas, you may want to check out ideas
from previous years and contact the mentors for those projects to see if
they're still interested in mentoring someone this year.

_Note_: Do not submit a proposal for an idea from a previous year
without contacting the mentor to ensure they will be available to mentor
you. *Without a mentor, proposals will be rejected.*
////
TODO - I don't know what to do with this yet.

Previous years:

* link:Summer_coding_ideas_for_2016[2016]
* link:Summer_coding_ideas_for_2015[2015]
* link:Summer_coding_ideas_for_2014[2014]
* link:Summer_coding_ideas_for_2013[2013]
* link:Summer_coding_ideas_for_2012[2012]
* link:Summer_coding_ideas_for_2011[2011]
* link:Summer_coding_ideas_for_2010[2010]
* link:Summer_coding_ideas_for_2009[2009]
* link:Summer_coding_ideas_for_2008[2008]
////
