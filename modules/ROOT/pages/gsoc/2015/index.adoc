= Google Summer of Code 2015
image:Gsoc-2015-logo.jpg[]

The Fedora project represented the Google Summer of Code program for 9
years and participating on year 2016 program as well. This wiki page
serves as the GSoC portal. Please feel free to contact us via list for link:https://lists.fedoraproject.org/admin/lists/summer-coding@lists.fedoraproject.org/[summer-coding]
clarifications/ more info, or the IRC channel: link:https://webchat.freenode.net/?channels=#fedora-summer-coding[#fedora-summer-coding] .

[[students]]
== Students


Students who are looking for challenges and would like to contribute to
the worlds' leading and innovative Linux Distro, this could be the
chance. Please feel free to contact and refer to the material and start
contacting mentors.

1.  xref:ideas.adoc[Main Idea Page]

[[why-spend-your-summer-working-on-foss]]
=== Why spend your summer working on FOSS?


When you work in the open on free software, you create a body of work
that follows you for the rest of your life. Rather than a coding
assignment done by thousands of other students and relegated to the
bottom of the bit drawer at semester's end, working in FOSS is a chance
to contribute to a living project.

Working in FOSS gives you a chance to:

* Work with real world large codebases.
* Collaborate with real engineers and other professional experts.
* Contribute to something meaningful while learning and earning student
  value.
* Learn tools and processes that are just like what you are going to use
  if you work in technology after graduation.
* Make friends and contacts around the globe.
* Possibly attract attention that gets you an internship or job after
  graduation.
* Create life time connections.

[[why-work-with-fedora]]
=== Why work with Fedora?

Our project is large and diverse. We are very experienced at working
with new contributors and helping them be successful.

Many long-time contributors continue to be around, lending expertise and
mentoring. People who stay around the community and do good work are
noticed. They get hired for jobs from it, including being hired by Red
Hat. Past Google Summer of Code students were hired by Red Hat, as well
as interns in various positions. This is just an example, as experience
and reputation in the Fedora Project communities is influential on your
career in many ways.

As long-standing communities with many facets, it is possible for you to
find many rewarding sub-projects to work on.

You should know that contributing to FOSS doesn't require you to have
super programming skills, or super-anything else. You just need be
interested and curious enough, and be willing to become comfortable
being productively lost. This is the state of learning through finding
your way around and figuring things out.

[[step-by-step-guide-for-students]]
=== Step-by-step guide for students


Please check the link:https://fedoraproject.org/wiki/GSOC_Guide_students[Step by Step guide for
students].

[[students-application]]
=== Student's application


Please refer to the following to follow the students' application
process,

xref:student_application_process.adoc[Student Application Process]

[[administration]]
== Administration

In order to clarify matters/ obtain more info related with the GSoC 2016
with Fedora please contact the administrators directly (please consider
CCing the summer-coding list where ever possible).

1.  link:https://fedoraproject.org/wiki/User:Sgallagh[Stephen Gallagher]
2.  link:https://fedoraproject.org/wiki/User:kushal[Kushal Das]
3.  link:https://fedoraproject.org/wiki/User:hguemar[Haïkel Guémar]

[[mentors]]
== Mentors


The contributors of the Fedora Project can propose ideas and mentor
them. Please feel free to check following links and please add your
ideas to the main idea page, further if you are not interested in
proposing an idea but still want to support the program please check the
students' idea page and pick one as per your interest.

1.  http://en.flossmanuals.net/GSoCMentoring/[Manual on Mentoring]
2.  xref:ideas.adoc[Main Idea Page]

[[how-to-work-with-students]]
=== How to work with students

* One way is to provide an idea for students to work on. This idea might
  be very well planned out, in which case you may need a high-level of
  contact with the student to get it implemented correctly.

* It is harder to find success where you are completely certain of how
  an idea needs to be implemented; finding a student with the skills and
  interest to implement a specific solution is a lot harder than finding a
  student with enough skills to respond to a use case need.

* Where you can have looser ideas, you may be able to find a student who
  works as a sort-of intern who can implement a solution to a use case you
  have. In past experiences, students going after a use case are more
  likely to get somewhere with self-direction.

* You may also want to work with a student who brings an idea to your
  sub-project. This requires a different level of communication throughout
  the project, but can be the most rewarding.

[[mentor-responsibilities]]
=== Mentor responsibilities


You are an essential part of the student's success, the project's
success, and the success for your overall organization (Fedora,
JBoss.org, or another).

Your responsibilities include:

* Being an interface for an identified sub-project or SIG in Fedora.
* Helping students communicate with the overall project and any
  upstream.
* Be the final, accountable person for deciding if the student is
  successful or not, which affects payment.

[[list-of-mentors]]
=== List of Mentors


[[list-of-registered-members]]
==== List of registered members

List of mentors who are registered with Google and added to the project
as a mentor are listed here;

link:https://fedoraproject.org/wiki/GSOC_2016/Registered_Mentors_List[Registered Mentor List]

[[communication]]
== Communication

*Mailing List (GSOC related) :* link:https://lists.fedoraproject.org/admin/lists/summer-coding@lists.fedoraproject.org/[summer-coding] +
*Mailing List (Technical) :* link:https://lists.fedoraproject.org/admin/lists/devel@lists.fedoraproject.org/[devel]  +
*IRC :* Channel - link:https://webchat.freenode.net/?channels=#fedora-summer-coding[#fedora-summer-coding] or link:https://webchat.freenode.net/?channels=#fedora-devel[#fedora-devel] on Freenode

[[time-line-student-version]]
== Time Line (Student version)


Reference :
http://www.google-melange.com/gsoc/events/google/gsoc2016[Full timeline]

* 6 October, 2014: Program announced.
* 9 February, 2015: 19:00 UTC Mentoring organizations can begin
  submitting applications to Google.
* 20 February: 19:00 UTC Mentoring organization application deadline.
* 23 - 27 February: Google program administrators review organization
  applications.
* 2 March: 19:00 UTC List of accepted mentoring organizations published
  on the Google Summer of Code 2015 site.
* Interim Period: Would-be students discuss project ideas with potential
  mentoring organizations.
* 6 March 16:00 UTC: IRC feedback meeting for rejected organizations for
  Google Summer of Code 2015. #gsoc on freenode.net. Rejected
  organizations may also choose to email the administrators directly for
  feedback.
* 14 March: 19:00 UTC Student application period opens.
* 27 March: 19:00 UTC Student application deadline.
* Interim Period: Mentoring organizations review and rank student
  proposals; where necessary, mentoring organizations may request further
  proposal detail from the student applicant.
* 13 April: Mentoring organizations should have requested slots via
  their profile in Melange by this point.
* 15 April: Slot allocations published to mentoring organizations.
* Interim Period: Slot allocation trades happen amongst organizations.
  Mentoring organizations review and rank student proposals; where
  necessary, mentoring organizations may request further proposal detail
  from the student applicant.
* 21 April: First round of de-duplication checks happens; organizations
  work together to try to resolve as many duplicates as possible.
* 24 April: All mentors must be signed up and all student proposals
  matched with a mentor -07:00 UTC. Student acceptance choice deadline.IRC
  meeting to resolve any outstanding duplicate accepted students - 19:00
  UTC #gsoc (organizations must send a delegate to represent them in this
  meeting regardless of if they are in a duplicate situation before the
  meeting.)
* 27 April: 19:00 UTC Accepted student proposals announced on the Google
  Summer of Code 2015 site.

Community Bonding Period: Students get to know mentors, read
documentation, get up to speed to begin working on their projects.

* 25 May: Students begin coding for their Google Summer of Code
  projects;

Google begins issuing initial student payments provided tax forms are on
file and students are in good standing with their communities. Work
Period: Mentors give students a helping hand and guidance on their
projects.

* 26 June: 19:00 UTCMentors and students can begin submitting mid-term
  evaluations.
* 3 July: 19:00 UTC Mid-term evaluations deadline;

Google begins issuing mid-term student payments provided passing student
survey is on file. Work Period: Mentors give students a helping hand and
guidance on their projects.

* 17 August: Suggested 'pencils down' date. Take a week to scrub code,
  write tests, improve documentation, etc.
* 21 August: 19:00 UTC Firm 'pencils down' date. Mentors, students and
  organization administrators can begin submitting final evaluations to
  Google.
* 28 August: 19:00 UTC Final evaluation deadline

Google begins issuing student and mentoring organization payments
provided forms and evaluations are on file.

* 28 August: 19:30 UTC Students can begin submitting required code
  samples to Google
* 31 August: Final results of Google Summer of Code 2015 announced
* 25 September: 19:00 UTC "Soft" deadline for student code sample
  submission. Students who want their t-shirts and certificates in the
  first wave of shipments must submit their code sample by this date.
* 6 - 8 November: Mentor Summit at Google: Delegates from each
  successfully participating organization are invited to Google to greet,
  collaborate and code. Our mission for the weekend: make the program even
  better, have fun and make new friends.

[[ideas-page]]
== Ideas Page


*Status :* Open for Ideas +
*Link :* xref:ideas.adoc[Summer coding ideas for 2015]

We encourage students to provide creative yet useful ideas towards the
Fedora project as well. Please use
link:https://fedoraproject.org/wiki/Summer_coding_ideas_for_2016/Students_Idea[Student Idea] page to
note your idea. The idea will be moved to the original idea page once
the idea is picked by a mentor.

[[policy-on-proposal-submissions-on-non-related-project-ideas]]
== Policy on proposal submissions on non-related project ideas


Over the past period we have observed that few students tend to submit
proposals for their own ideas directly to Google-melange. There will be
no communication after then even we ask more info on Google-melange.
Such nature of submissions need extra effort when it comes to manage
proposals on Google-Melange. Therefore as a policy, proposals for
students ideas (with no supporting mentor indicated) that are being
submitted directly to Google-melange will be marked *Ignore* after a 24
hours of warning unless the students does not voluntary withdraw the
proposal or explain basis of their idea. +
This will not restrict submitting proposals on students ideas, we always
welcome novel and innovative ideas from students if they follow the
right path. As the first step send us your idea and will start discuss
how to manage it. If you have a supporting mentor for your own idea
please feel free to submit it directly to Google-melange and clearly
note the mentors' contact details.

[[links]]
== Links


1.  link:https://fedoraproject.org/wiki/Foundation[The Foundation]
2.  http://www.google-melange.com/gsoc/document/show/gsoc_program/google/gsoc2016/help_page[GSoC
    FAQ]
3.  http://docs.fedoraproject.org/en-US/index.html[Fedora Documentation]
4.  link:https://fedoraproject.org/wiki/Communicate/IRCHowTo[IRC]
5.  link:https://fedoraproject.org/wiki/Development[Development]


