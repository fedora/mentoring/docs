
////
WARNING: Fedora is still applying to be a GSoC mentoring organization.  This page will be updated if we are accepted.  We are currently collecting possible project ideas here.
////

NOTE: Fedora is proud to have been accepted as a GSoC mentoring organization.  Student applications open on March 25, 2019.  Please make sure you carefully read through the xref:gsoc/2019/index.adoc[general information] and xref:gsoc/2019/application.adoc[application process] pages before applying.

If you are a student looking forward to participating in
xref:gsoc/2019/index.adoc[Google Summer of Code with Fedora], please feel free to
browse this idea list.  There may be additional ideas added during the
application period.

**Now please go read the xref:gsoc/2019/index.adoc#what-can-i-do-today[What
Can I do Today] section of the main page. This has the answers to your
questions and tells you how to apply**

Do not hesitate to contact the mentors or contributors listed on this
page for any questions or clarification. You can find helpful people on
the IRC channel, or use the mailing list. can be used for getting help
with programming problems.

== Supporting Mentors

The following contributors are available to provide general help and
support for the GSoC program If a specific project mentor is busy, you
can contact one of the people below for short-term help on your project
or task.
add yourselves and your wiki page).

* link:https://fedoraproject.org/wiki/User:Bex[Brian (bex) Exelbierd] (Fedora Community Action and Impact Coordinator, FCAIC, 🎂, containers, general development, general Linux)
* link:https://fedoraproject.org/wiki/User:Sumantrom[Sumantro Mukherjee] (General development, general Linux, Fedora community, GSoC alumnus, questions about program, misc. advice)
* link:https://fedoraproject.org/wiki/User:Bt0dotninja[Alberto] (Commops/Fedora-Join/Marketing teams)

== Idea list
NOTE: Ideas are subject to change as additional mentors are onboarded.

* <<CentOS CI user front end>>
* <<Improving Fedora Android App>>
* <<Fedora Gooey Karma>>
* <<Change management tool>>
* <<Podman Container SECCOMP generation tool>>
* <<Release-bot>>

=== CentOS CI user front end

- Difficulty : Intermediate
- Technology : HTML5, CSS3, Angular, Javascript, TBD.
- Mentor :  https://fedoraproject.org/wiki/User:Bstinson[Brian Stinson], https://fedoraproject.org/wiki/User:Siddharthvipul1[Vipul Siddharth]
- IRC & Email : Brian Stinson [IRC: bstinson, bstinson@redhat.com],
  Vipul Siddharth [IRC: siddharthvipul, vsiddhar@redhat.com]


==== Description

The CentOS Project runs a public CI resource that is available to all infra and infra related open source projects to come and consume. The problem is that the signup process is very manual, both for the incoming user and the CICO admins. CentOS CI is powered by OKD (https://okd.io), and we would like to have a web application that handles the login, Admin approval, and OKD project creation (from a template) steps and also displays links to active CICO namespaces.

Read more :

- https://wiki.centos.org/QaWiki/CI
- https://wiki.centos.org/QaWiki/CI/GettingStarted

==== Deliverables
As a GSoC intern, you will be responsible for the following :

- Engage in community discussion and do community research
- Build initial mockups and gather community feedback
- Finalize design and start making them functional
- Follow UX/UI standards to build the best possible experience users

'''

=== Improving Fedora Android App

- Difficulty : Intermediate
- Technology : Android, HTML, CSS, Cordova, Angular, Javascript, Ionic
- Mentor :  Amitosh Swain Mahapatra [amitosh@fedoraproject.org]


==== Description

Fedora has an android app which lets a user browse the magazine, Fedora Ask, browse the calendar etc within it.

Repo: https://pagure.io/Fedora-app

Goals :

- Make the app available on other platforms
- Implement new integrations based on community interest

==== Deliverables
As a GSoC intern, you will be responsible for the following :

- iOS port of the app (publishing on app store is not a deliverable)
- App backend for interfacing with 3rd party APIs to API limits
- Integrate with FMN
- Search and integrate with more Fedora services

'''

=== Fedora Gooey Karma

- Difficulty : Intermediate
- Technology :  Python, Rest API, Packaging
- Mentor :  Sumantro Mukherjee [Sumantro@redhat.com]


==== Description

The Fedora QA team is seeking help to fix tooling which enables testers and lot of contributors post karmas against updates. Fedora uses Bodhi to track the positiveness or negativeness of a package with respect to its functionality and overall system performance.
The student will be working to refactor and rewrite the tool from scratch if required. Currently, the code is hosted at https://pagure.io/fedora-qa/fedora-gooey-karma

==== Deliverables
As a GSoC intern, you will be responsible for the following :

- Look for Community Feedback and work on features
- Application ported to Bodhi2 API and Python3
- Revamp the UI built with QT
- Package and ship

'''

=== Change management tool

- Difficulty : Intermediate
- Technology :  Python, REST, HTML, CSS
- Mentor :  https://fedoraproject.org/wiki/User:Bcotton[Ben Cotton]
- IRC & Email :  bcotton | bcotton@fedoraproject.org


==== Description

Fedora's Change process currently involves a lot of manual work to move proposals through the process. Information is copy/pasted into a variety of platforms, which is tedious and error-prone.

What are we looking for:

- Work with Fedora Program Manager to fit scripts to Change process
- Work with FESCo, FPL, and community members to determine what reports are useful

Resources :

- https://fedoraproject.org/wiki/Changes/Policy[Change Process]
- https://fedoraproject.org/wiki/User:Bcotton/UsePagureForChanges[Update Process]



==== Deliverables
As a GSoC intern, you would write a Python script or scripts that would:

- Pull data from the Change source (a Taiga board or Pagure issue, TBD) and send email
- Submit issues to other appropriate trackers (e.g. FESCo for approval)
- Update Change source with current state of proposal
- Produce static HTML reports

'''
=== Podman Container SECCOMP generation tool

- Difficulty : Intermediate
- Technology :  Golang, Containers, Podman, Linux
- Mentor :  Dan Walsh, Valentin Rothberg

==== Contact
- Dan Walsh [IRC: dwalsh, mail: dwalsh@redhat.com, twitter: rhatdan, github: rhatdan],
- Valentin Rothberg [IRC: vrothberg, mail: rothberg@redhat.com, twitter: vlntnrthbrg, github: vrothberg]


==== Description

Most containers currently have a hard-coded default seccomp profile, that is pretty loose and meant to support a wide range of use-cases. The idea of this project is to build a tool that would watch all of the syscalls made within a container, and generate a seccomp profile for this specific container to further harden security. We would want to add a command to the Pod Manager (Podman) tool to basically launch the container and then collect a set of syscalls either through strace, or auditing, or similar tracing technologies.

Repo : https://github.com/containers/libpod

==== Deliverables
As a GSoC intern you will be responsible for :

- Engage in community discussions
- Research how syscalls for a given workload (i.e., container) can be automatically traced (e.g., via strace)
- Implement a prototype based on Podman
- Collaborate with the mentors and the community to integrate the functionality upstream

'''
===  Release-bot

- Difficulty : Intermediate
- Technology :   Python
- Mentor :  Rado Pitonak [IRC rpitonak, mail: rpitonak@redhat.com] ,
  Tomas Tomecek [IRC ttomecek, mail: ttomecek@redhat.com]

==== Description

Release-bot helps upstream maintainers deliver their software to users, via automated releases at GitHub and PyPI.  Right now there are many ways how workflow of release-bot can be improved:

- Make release-bot available as GitHub app
- Listen to GitHub callbacks instead of polling GitHub API in endless loop
- Improve end to end integration test suite
- Make release-bot "smarter" in various ways (e.g communication with maintainer)
- We are open to your new ideas

Repo : https://github.com/user-cont/release-bot

==== How to start
If you are interested to start working on this project, please see our  https://github.com/user-cont/release-bot/issues[issue tracker]. Choose an issue to work on and open PR. Issues that are newcomers friendly are labeled with `good first issue label`.
Good code contributions during application period highly increasing your chance to be selected.
