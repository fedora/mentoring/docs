

NOTE: Fedora is proud to have been accepted as a GSoC mentoring organization.
////
NOTE: Fedora is proud to have been accepted as a GSoC mentoring organization.  Student applications open on March 25, 2019.  Please make sure you carefully read through the xref:gsoc/2019/index.adoc[general information] and xref:gsoc/2019/application.adoc[application process] pages before applying.
////
If you are a student looking forward to participating in
xref:gsoc/2020/index.adoc[Google Summer of Code with Fedora], please feel free to
browse this idea list.  There may be additional ideas added during the
application period.

**Now please go read the xref:gsoc/2019/index.adoc#what-can-i-do-today[What
Can I do Today] section of the main page. This has the answers to your
questions and tells you how to apply**

Do not hesitate to contact the mentors or contributors listed on this
page for any questions or clarification. You can find helpful people on
the IRC channel, or use the mailing list. can be used for getting help
with programming problems.

== Supporting Mentors

The following contributors are available to provide general help and
support for the GSoC program If a specific project mentor is busy, you
can contact one of the people below for short-term help on your project
or task.
add yourselves and your wiki page).


* link:https://fedoraproject.org/wiki/User:Sumantrom[Sumantro Mukherjee] (General development, general Linux,
  Fedora community, GSoC alumnus, questions about program, misc. advice)
* link:https://fedoraproject.org/wiki/User:Siddharthvipul1[Vipul Siddharth] (Fedora CI,GCI,GSoC,general linux,Fedora community, misc.)

== Idea list
NOTE: Ideas are subject to change as additional mentors are onboarded.

* <<Packit can work with Gitlab>>
* <<Dashboard for Packit>>
* <<Improve Network Linux System Role>>
* <<Add varlink support to Nmstate>>

=== Packit can work with Gitlab

- Difficulty : Intermediate
- Technology : Python, Flask, Container basics, REST API
- Mentor :  Frantisek Lachman
- IRC & Email : Frantisek Lachman [IRC: lachmanfrantisek, flachman@redhat.com]


==== Description

Packit (https://packit.dev/) project has native support exclusively for GitHub now. There were
numerous requests from the community to add support for GitLab as well. So here
we are - are you up for the challenge to implement support for GitLab in
packit-service (https://github.com/packit-service/packit-service) so that it can process events from GitLab API and return
feedback of successful builds or failed tests back into the merge request?
Please bear in mind that GitLab and GitHub use slightly different workflows
(pipelines vs. external CI systems) so design work needs to be done before
writing code.

Resources can be found at Upstream tracking issue: https://github.com/packit-service/packit-service/issues/249

==== Deliverables
As a GSoC intern, you will be responsible for the following :

* Code in Packit service repository which makes packit understand the gitlab events
* Packit is able to talk to a GitLab server API and:
** Comment on merge request
** Report check results (builds, tests)
* On-board at least a single project from GNOME gitlab instance (https://gitlab.gnome.org/explore/groups) to use packit.

'''

===  Dashboard for Packit

- Difficulty : Intermediate
- Technology : Python, Flask, Container basics, REST API, webdesign (CSS & javascript)
- Mentor :  Hunor Csomortáni
- IRC & Email : Hunor Csomortáni [IRC: csomh, csomh₊gsoc@redhat.com]


==== Description

Packit project currently has an extremely basic dashboard (https://github.com/packit-service/dashboard/) with very little information in it. We’d love to have more tables, more data, charts and all the shiny stuff, in order to make it easier to get an insight of how the service operates and how users interact with it.

We’re looking for these views:

- Listing for recent build and test runs
- The same listing but per project
- A chart of usage - how many tasks are executed in a given time of a day
- Details for projects and builds

This task has a challenge - our API doesn’t provide most of the data needed to accomplish this. So the expectation here is to work with the upstream project (us) to come up with a final design, provide the data and develop the dashboard.

==== Deliverables
As a GSoC intern, you will be responsible for the following :

- Code in packit-service/dashboard with the functionality
- OpenShift templates and Ansible playbooks in packit-service/dashboard,deployment so that we can deploy it in our openshift cluster
- Tests to verify that the proposed code works
- The updated dashboard deployed in production

'''

===  Improve Network Linux System Role

- Difficulty : Intermediate
- Technology : Python, Ansible
- Mentor :  Till Maas, Thomas Haller
- IRC & Email : tyll, till q redhat.com / thaller, thaller q redhat.com


==== Description

The Network Linux System Role provides a uniform configuration interface for network-scripts and NetworkManager. In this project, the role would be improved. There are several areas that could be selected by an intern depending on their interest:

* Add support for more interfaces/options, this includes the following tasks
    . Write missing tests/documentation for open PRs to finalize them
    . Add support for wake-on-lan options:    https://github.com/linux-system-roles/network/issues/150
    . Add proper support for team and make the configuration more uniform with bonding

* Improve the testing framework
    . Write an integration test to become familiar with the role, for example for https://github.com/linux-system-roles/network/issues/124
    . Make the tests more uniform (they developed over the time. Adjust old tests to use the conventions of newer tests)
    . Add support to test the ansible module directly via pytest instead of only via ansible-playbook
    . Simplify test playbooks by writing custom Ansible modules that simplify test setup/preparation and assertions

* Network state management (most difficult)
    . Support to update only partial settings
    . Return the current network configuration
    . Initially only the configuration files
    . Maybe also the runtime state



* What are we looking for:
    . Interest in writing high-quality code in Python for Ansible
    . Personal accountability with regular, clear and open communication
    . Ability to independently transfer feedback into code
    . Support for more features or a better test framework
    . Effective collaboration via GitHub, IRC and video conferences

* Notes & references:
     . https://github.com/linux-system-roles/network/
. Current tests: https://github.com/linux-system-roles/network/tree/master/tests


'''

===   Add varlink support to Nmstate

- Difficulty : Intermediate
- Technology : Python, Linux shell, REST api, LInux Networking (basic)
- Mentor :  Fernando Fernandez Mancera, Till Maas (Helping with mentoring) and Gris Ge (Helping with mentoring).
- IRC & Email :ffmancera, ferferna@redhat.com

==== Description

Nmstate is a python library (libnmstate) and a command line tool (nmstatectl) that manages host networking settings in a declarative manner. https://nmstate.github.io/

There are some users that want to run nmstate in systems that do not have or do not support python. In order to solve this issue, the student would implement the varlink support to use nmstate from other languages. https://varlink.org/

Till have implemented a Proof of Concept for this task so the user is able to use it to understand how varlink works and how to proceed. https://github.com/tyll/varlink-nmstate

Ideally, kubernetes-nmstate should be updated to use varlink. This is not a must.

==== Deliverables
As a GSoC intern, you will be responsible for the following :

- Code of the varlink nmstate support
- Basic usage documentation (User README)
- Code documentation (Not very detailed, just general comments on how is the code structured)


'''
