Subject: GSoC Mentor Questions

Hi,

You are receiving this email because you have listed yourself as a
project mentor on the GSoC17 Idea page for Fedora.

Thank you for offering to mentor a GSoC student.  I am hopeful that your
project will attract many great students.

This is my first year working as a GSoC administrator.  When I was
asking for advice, one thing I got told was that we have had a problem
in the past with some mentors not realizing what is involved in
mentoring.  Therefore I am hoping you can answer some questions to
ensure we are on the same page:

1. Have you read your mentor responsibilities (from:
   https://developers.google.com/open-source/gsoc/help/responsibilities#mentor=_responsibilities)?
   [Also included below]

   Do you have any questions?  Any concerns?

2. Are you available for enough time during the community bonding period
   (4-30 May 2017) and the mentoring period (30 May - 21 August, 2017)?
   You should plan to have an interaction with your mentee at least every
   week, and often multiple times per week.  Students require a lot more
   guidance than many people realize.  The goal, as you know, is to both
   get great code and to create a great experience that fosters growth
   for the student.

   To add some additional perspective, Google suggests that "[m]entors
   should expect to spend about 4-5 hours a week for each student. Some
   weeks may be more, some weeks less. More time spent during the
   community bonding period and the first few weeks of the program have
   led to more successful and engaged students."

3. What is your level of involvement with the upstream of the project
   you are mentoring?

4. I'd like to have a monthly mentoring call.  This would be a real-time
   call scheduled for a hopefully convenient time for your timezone.
   The goal is to share what is working and isn't working from a
   mentoring perspective.  To that end, what timezone are you in?
   When is typically a good time for a 30-45 minute call?

5. Have you ever been a GSoC mentor before?  Have you ever worked with
   or been an intern or participated in another mentoring situation?

6. Is there anything else I should know so that I can help you be
   successful?

7. In order to register you are a mentor, I need to get your email
   address (for use with a Google system).  I am not able to register
   to you until you answer these questions.

Finally, if you haven't already, please read the GSoC mentor manual.
Google has done a great job of providing resources to help us all
succeed.

http://write.flossmanuals.net/gsoc-mentoring/what-makes-a-good-mentor/

Thank you.

regards,

bex

---

Mentor Responsibilities
...to your Org Admin

    Communicate availability and interaction expectations
    Inform when mentoring capacity will be reduced, as early as possible
    (e.g., family, health, vacation)
    Inform when there is an issue with a student
        Lacking communication, activity, visibility (MIA), or progress
        Participant Agreement violations (e.g., plagiarism, harassment,
        fraud)
        Bad fit or stepping down
    Formally evaluate student participation
        GSoC: Communicate with admin and student before failing

...to your Students

    Help and/or teach the student how to
        be a part of your community
        communicate more effectively and in the open
        work with your org=E2=80=99s preferred communication channel (IRC,
        Slack, etc)
        use your org=E2=80=99s version control system
        ask good questions and get answers to their questions
        provide convincing technical argument and constructive
        discussion
        be independently motivated and productive
        solve difficult technical problems
    Keep track of their progress, keep student informed as to their
    status
    Communicate on a regular basis, once a week or better (for GSoC)
        Give constructive feedback, be patient (particularly for GCI),
        and be respectful
        Respond to questions within 24 hours (occasionally under 36
        hours is ok)
    Establish realistic work objectives and timeline expectations
    Re-evaluate scope with student when significantly ahead of or behind
    expectations
        GCI: Give them extra time on a task as warranted
    Work with devs and community to facilitate acceptance of student
    work

