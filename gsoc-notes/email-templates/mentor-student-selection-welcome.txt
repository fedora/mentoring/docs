Thank you for contributing to Fedora as a mentor for Google Summer of Code.

tl;dr: Please take the time to do the following:

* Review the applicant(s) for your project.  Coordinate with any other mentors on the project and determine if any of the applicants are qualified.  If you have qualified applicants, select the best applicant.
* Read the Google Summer of Code mentor guide[1] and the Roles and Responsibilities doc[2].
* Consider watching the 5 minute YouTube video: "Being a Great GSoC Mentor"[3]. The video was shot with GSoC veteran Org Admins and Mentors. It contains helpful information about the program and how to participate successfully at each step in GSoC.

# Reviewing Applicants

Students must submit their final proposal as a PDF through the website. You should make your decisions on which students to accept based on the contents of the final PDF proposal and your interactions with the student.  Coordinate with any other mentors on the project and determine if any of the applicants are qualified.  If you have qualified applicants, select the best applicant.

Do not accept "okay" proposals.

Every student must have at least one mentor.  If you have more than one mentor available for the project, we can take multiple students if there is enough independent work.  When you become a mentor you are committing to a lot of work.  Make sure you're prepared for this and that you've told anyone who may be impacted (family, boss, etc.)

If you are interested in being a mentor for a student for a given proposal then you can click the purple "Want to Mentor" button to let me know that you are interested in this specific student. This is how I will set the slot request.

In general no one may mentor more than one student (in this program or in other programs [i.e. Outreachy, a work intership program, etc.]).  If you are in a situation where you feel like this shouldn't apply to you, let me know.

# Important Dates

April 8, 16:00 UTC: Deadline to nominate students for the program in the GSoC interface
April 9, 16:00 UTC: bex's Deadline to submit slot requests
April 10 - 17 16:00 UTC :  Select the proposals to become student projects. At least 1 mentor must be assigned to each project before it can be selected. I'll work with you all if we don't get enough slots.
April 23:  Accepted GSoC 2018 students/projects are announced
April 23 - May 14:  Community Bonding Period

# Mentors Mailing List

If you aren't already, please consider joining the mentor mailing list. To be included on the GSoC mentors mailing list be sure to opt in on your User Profile page. You can always opt out once you have been added by unsubscribing using the link on any of the emails.

1: https://google.github.io/gsocguides/mentor/
2: https://developers.google.com/open-source/gsoc/help/responsibilities
3: https://www.youtube.com/watch?v=3J_eBuYxcyg
