About Fedora

Fedora is a Linux-based free software operating system. You can use Fedora in
addition to, or instead of, other operating systems such as Microsoft Windows™
or Mac OS X™. The Fedora operating system is completely free of cost for you to
enjoy and share.

The Fedora Project is the name of a worldwide community of people who love, use,
and build free software. We want to lead in the creation and spread of free
code and content by working together as a community. Fedora is sponsored by Red
Hat, the world's most trusted provider of open source technology. Red Hat
invests in Fedora to encourage collaboration and incubate innovative new free
software technologies.

We believe in the value of free software, and fight to protect and promote
solutions anyone can use and redistribute. Not only is the Fedora operating
system made from free software, but we use free software exclusively to provide
it to you. The website you are reading this on right now, in fact, is made from
free software and serves millions of people every month.

We also believe in the power of collaboration. Our contributors work with free
software project teams around the world we call "upstream." They create much of
the software found in Fedora. We collaborate closely with them so everyone can
benefit from our work, and get access to improvements as soon as possible. By
working in the same direction as these project teams, we can ensure that free
software works better together, and provides the best experience for users. We
also can include improvements quickly, which helps not only users, but the
upstream as well.
