Organization Application for 2019 Season of Docs

1. Open source organization's email address *

mentored-projects-admin@lists.fedoraproject.org

2. Open source project name *

Fedora Project

3. Link to the open source project *

https://fedoraproject.org/

4. Open source project description *

The Fedora Project is a community of people working together to build a free and open source software platform and to collaborate on and share user-focused solutions built on that platform. Or, in plain English, we make an operating system and we make it easy for you do useful stuff with it.

5. Link to your organization’s page about Season of Docs

https://docs.fedoraproject.org/en-US/mentored-projects/gsod/2019/

6. Does your organization want to accept the mentor stipend? *

Yes

7.  What previous experience has your organization had in documentation or collaborating with technical writers?

The Fedora Project is the upstream of Red Hat Enterprise Linux.  For this reason we have routinely had technical writers participating in our community as Red Hat has supported documentation for many code projects it has committed.  Additionally, we have a part of a professional technical writer’s time provided by Red Hat to the Fedora Project to help us with documentation coordination and some content work.  Finally, our most recent collaboration was with a hired technical writing contractor who helped us updated and extended our Fedora IoT documentation at https://docs.fedoraproject.org/en-US/iot/

8. What previous experience has your organization had mentoring individuals?

Fedora is a diverse project and participates in mentoring activities like Google Summer of Code, Google Code In and Outreachy. The project has a contributor documentation team which is community driven. Petr Bokoč leads the documentation efforts inside the project along with assistance from Adam Šamalík on the build system. 

Documentation is an important part of our project.  We include a representative of this team in our Mindshare efforts.  This keeps documentation connected with other user facing efforts of our community.  The goal of this representative is to serve as a conduit between all of these groups and documentation.  We have also restarted efforts on translated documentation to enable greater access to our finished docs.

9. How many technical writers does your organization want to mentor this year? *

1

10. Link to project ideas list *

https://docs.fedoraproject.org/en-US/mentored-projects/gsod/2019/ideas/
